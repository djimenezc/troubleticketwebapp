package com.tektronix.troubleticket.webgui.servlet;

import com.tektronix.troubleticket.webgui.servlet.util.AgentType;
import com.tektronix.troubleticket.webgui.servlet.util.ClientAgent;
import org.junit.Test;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * Copyright (c) 2013 Tekcomms
 * <p/>
 * Trouble Ticket Integration Project
 * <p/>
 * User: gleeson
 * Date: 28/11/13
 * Time: 16:52
 * <p/>
 * This Class/Interface ....
 */
public class TestBrowserIdentifier {

    // Mix of IE types and versions
    private String[] ie = {
            "Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.1; WOW64; Trident/6.0)",
            "Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.1; Trident/6.0)",
            "Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.1; Trident/5.0)",
            "Mozilla/5.0 (Windows; U; MSIE 9.0; WIndows NT 9.0; en-US)",
            "Mozilla/5.0 (Windows; U; MSIE 9.0; Windows NT 9.0; en-US)",
            "Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 7.1; Trident/5.0)",
            "Mozilla/5.0 (compatible; MSIE 8.0; Windows NT 6.1; Trident/4.0; GTB7.4; InfoPath.2; SV1; .NET CLR 3.3.69573; WOW64; en-US)",
            "Mozilla/5.0 (compatible; MSIE 8.0; Windows NT 6.0; Trident/4.0; WOW64; Trident/4.0; SLCC2;)",
            "Mozilla/5.0 (compatible; MSIE 8.0; Windows NT 6.0; Trident/4.0; InfoPath.1; SV1; .NET CLR 3.8.36217; WOW64; en-US)"
    };
    // Mix of types and versions
    private String[] firefox = {
            "Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:25.0) Gecko/20100101 Firefox/25.0",
            "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.6; rv:25.0) Gecko/20100101 Firefox/25.0",
            "Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.9.1.16) Gecko/20120421 Gecko Firefox/11.0",
            "Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.9.1.16) Gecko/20120421 Firefox/11.0",
            "Mozilla/5.0 (X11; U; Linux x86_64; pl-PL; rv:2.0) Gecko/20110307 Firefox/4.0"
    };


    // Mix of types and versions
    private String[] chrome = {
            "Mozilla/5.0 (Windows NT 6.2; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1667.0 Safari/537.36",
            "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1664.3 Safari/537.36",
            "Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/31.0.1650.16 Safari/537.36",
            "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/31.0.1623.0 Safari/537.36",
            "Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/30.0.1599.17 Safari/537.36",
            "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.62 Safari/537.36",
            "Mozilla/5.0 (X11; CrOS i686 4319.74.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.57 Safari/537.36"
    };

    private String[] unknowns = {
         "Mozilla/5.0 (compatible; Konqueror/3.1-rc1; i686 Linux; 20021226)",
         "Mozilla/5.0 (compatible; Konqueror/3.1-rc1; i686 Linux; 20021221)",
        "Mozilla/5.0 (compatible; Konqueror/3.1-rc1; i686 Linux; 20021120)"
    };



    @Test
    public void testBrowserTypes() throws Exception {
        assertTrue("IE not identified correctly",agentCorrectlyIdentified(ie, AgentType.InternetExplorer));
        assertTrue("Firefox not identified correctly",agentCorrectlyIdentified(firefox, AgentType.Firefox));
        assertTrue("Chrome not identified correctly",agentCorrectlyIdentified(chrome, AgentType.Chrome));
        assertTrue("Unknowns not identified correctly",agentCorrectlyIdentified(unknowns, AgentType.Unknown));
    }

    @Test
    public void testBrowserFields() throws Exception {
       ClientAgent agent = new ClientAgent(ie[0]);
       assertTrue("Not identified correctly ",agent.getAgentType()==AgentType.InternetExplorer);
       assertNotNull("Agent String empty",agent.getAgent());
       assertNotNull("Agent Version String empty",agent.getAgentVersion());
    }


    private boolean agentCorrectlyIdentified(String[] agentStrings, AgentType agentType){
        for(String a: agentStrings ){
            ClientAgent c = new ClientAgent(a);
            if( c.getAgentType() != agentType ){
              return false;
            }
        }
        return true;

    }


}
