package com.tektronix.troubleticket.webgui.servlet;

import org.apache.commons.codec.binary.Base64;
import org.codehaus.jackson.map.ObjectMapper;
import org.junit.Test;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletInputStream;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

/**
 * Copyright (c) 2013 Tekcomms
 * <p/>
 * Trouble Ticket Integration Project
 * <p/>
 * User: gleeson
 * Date: 22/11/13
 * Time: 15:12
 * <p/>
 * This Class/Interface ....
 */
public class TestBase64encoder {

    private ObjectMapper objectMapper = new ObjectMapper();
    // Lines should end with CRLF

    private static final String BOUNDARY_CODE = "AaB03x";
    static final String MULTIPART_FORM= "\r\n"
                    + "$$CONTENT$$"
                    + "--" + BOUNDARY_CODE + "--\r\n";
    static final String MULTIPART_FILE= "--" + BOUNDARY_CODE + "\r\n"
            + "Content-Disposition: form-data; name=\"$$N$$\"; filename=\"$$FILENAME$$\"\r\n"
            + "Content-Type: application/octet-stream\r\n"
            + "\r\n"
            + "$$PAYLOAD$$\r\n\r\n";


    /**
     * We do a vanilla post - we should get a 'success' response
     */
    @Test
    public void testPlainPost() throws Exception {
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
        StubServletOutputStream outputStream = new StubServletOutputStream();
        ServletConfig servletConfig = mock(ServletConfig.class);
        ServletContext servletContext = mock(ServletContext.class);
        when(servletConfig.getServletContext()).thenReturn(servletContext);
        when(servletConfig.getServletContext().getInitParameter("maxMemoryFileSize")).thenReturn("128000");
        when(servletConfig.getServletContext().getInitParameter("maxFileSize")).thenReturn("256000");
        when(servletConfig.getServletContext().getInitParameter("maxNumberFiles")).thenReturn("2");
        when(request.getMethod()).thenReturn("post");
        when(request.getContentType()).thenReturn("application/json");


        Base64EncoderServlet encoderServlet = new Base64EncoderServlet();
        encoderServlet.init(servletConfig); // Mock

        // Create data block
        byte[] inputStreamBytes = createDataBlock(128);

        // Setup the input and output streams
        when(request.getInputStream()).thenReturn(createServletInputStream(inputStreamBytes));
        when(response.getOutputStream()).thenReturn( outputStream );
        // Make call
        encoderServlet.doPost(request,response);
        // Verify mime returned
//        verify(response).setContentType("application/json"); // DISABLED until we get a fix on the issue
        // OK verify response - its JSON so use jackson to decode
        String jsonString = outputStream.data.toString();
        assertNotNull(jsonString);

        Map<String,Object> json = objectMapper.readValue(jsonString,Map.class);
        // Assert success present and has a value of success
        assertTrue("Success field missing in " + jsonString, json.containsKey("success"));
        assertTrue("Success should be true " + jsonString, ((Boolean) json.get("success")));

        List<Map<String,Object>> files = (List<Map<String, Object>>) json.get("files");
        assertNotNull("No Files encoded!!!",files);
        assertNotNull("Files enmpty!!!",files.size()>0);
        byte[] decoded = decodeBase64((String)files.get(0).get("base64Data"));
        // Now we need to verify what we sent was encoded correctly
        assertArrayEquals("Byte Mismatch", inputStreamBytes,decoded);

    }

    /**
     * We do a vanilla post - we should get a 'success' response
     */
    @Test
    public void testPlainPostKilobyteRange() throws Exception {
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
        StubServletOutputStream outputStream = new StubServletOutputStream();
        ServletConfig servletConfig = mock(ServletConfig.class);
        ServletContext servletContext = mock(ServletContext.class);
        when(servletConfig.getServletContext()).thenReturn(servletContext);
        when(servletConfig.getServletContext().getInitParameter("maxMemoryFileSize")).thenReturn("128000");
        when(servletConfig.getServletContext().getInitParameter("maxFileSize")).thenReturn("256000");
        when(servletConfig.getServletContext().getInitParameter("maxNumberFiles")).thenReturn("2");
        when(request.getMethod()).thenReturn("post");
        when(request.getContentType()).thenReturn("application/json");


        Base64EncoderServlet encoderServlet = new Base64EncoderServlet();
        encoderServlet.init(servletConfig); // Mock

        // Create data block
        byte[] inputStreamBytes = createDataBlock(100000);

        // Setup the input and output streams
        when(request.getInputStream()).thenReturn(createServletInputStream(inputStreamBytes));
        when(response.getOutputStream()).thenReturn( outputStream );
        // Make call
        encoderServlet.doPost(request,response);
        // OK verify response - its JSON so use jackson to decode
        String jsonString = outputStream.data.toString();
        assertNotNull(jsonString);

        Map<String,Object> json = objectMapper.readValue(jsonString,Map.class);
        // Assert success present and has a value of success
        assertTrue("Success field missing in " + jsonString, json.containsKey("success"));
        assertTrue("Success should be true " + jsonString, ((Boolean) json.get("success")));

        List<Map<String,Object>> files = (List<Map<String, Object>>) json.get("files");
        assertNotNull("No Files encoded!!!",files);
        assertNotNull("Files enmpty!!!",files.size()>0);
        byte[] decoded = decodeBase64((String)files.get(0).get("base64Data"));
        // Now we need to verify what we sent was encoded correctly
        assertArrayEquals("Byte Mismatch", inputStreamBytes,decoded);

    }

    /**
     * We do a vanilla post - we should get a 'success' response
     */
    @Test
    public void testPlainPostMegabytes() throws Exception {
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
        StubServletOutputStream outputStream = new StubServletOutputStream();
        ServletConfig servletConfig = mock(ServletConfig.class);
        ServletContext servletContext = mock(ServletContext.class);
        when(servletConfig.getServletContext()).thenReturn(servletContext);
        when(servletConfig.getServletContext().getInitParameter("maxMemoryFileSize")).thenReturn("128000");
        when(servletConfig.getServletContext().getInitParameter("maxFileSize")).thenReturn("256000");
        when(servletConfig.getServletContext().getInitParameter("maxNumberFiles")).thenReturn("2");
        when(request.getMethod()).thenReturn("post");
        when(request.getContentType()).thenReturn("application/json");


        Base64EncoderServlet encoderServlet = new Base64EncoderServlet();
        encoderServlet.init(servletConfig); // Mock

        // Create data block
        byte[] inputStreamBytes = createDataBlock(2000000);

        // Setup the input and output streams
        when(request.getInputStream()).thenReturn(createServletInputStream(inputStreamBytes));
        when(response.getOutputStream()).thenReturn( outputStream );
        // Make call
        encoderServlet.doPost(request,response);
        // OK verify response - its JSON so use jackson to decode
        String jsonString = outputStream.data.toString();
        assertNotNull(jsonString);

        Map<String,Object> json = objectMapper.readValue(jsonString,Map.class);
        // Assert success present and has a value of success
        assertTrue("Success field missing in " + jsonString, json.containsKey("success"));
        assertTrue("Success should be true " + jsonString, ((Boolean) json.get("success")));

        List<Map<String,Object>> files = (List<Map<String, Object>>) json.get("files");
        assertNotNull("No Files encoded!!!",files);
        assertNotNull("Files enmpty!!!",files.size()>0);
        byte[] decoded = decodeBase64((String)files.get(0).get("base64Data"));
        // Now we need to verify what we sent was encoded correctly
        assertArrayEquals("Byte Mismatch", inputStreamBytes,decoded);

    }


    /**
     *
     */
    @Test
    public void testPlainPostNoContent()  throws Exception{
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
        StubServletOutputStream outputStream = new StubServletOutputStream();
        ServletConfig servletConfig = mock(ServletConfig.class);
        ServletContext servletContext = mock(ServletContext.class);
        when(servletConfig.getServletContext()).thenReturn(servletContext);
        when(servletConfig.getServletContext().getInitParameter("maxMemoryFileSize")).thenReturn("128000");
        when(servletConfig.getServletContext().getInitParameter("maxFileSize")).thenReturn("256000");
        when(servletConfig.getServletContext().getInitParameter("maxNumberFiles")).thenReturn("2");
        when(request.getMethod()).thenReturn("post");
        when(request.getContentType()).thenReturn("application/json");


        Base64EncoderServlet encoderServlet = new Base64EncoderServlet();
        encoderServlet.init(servletConfig); // Mock

        // Create data block
        byte[] inputStreamBytes = new byte[0];

        // Setup the input and output streams
        when(request.getInputStream()).thenReturn(createServletInputStream(inputStreamBytes));
        when(response.getOutputStream()).thenReturn( outputStream );
        // Make call
        encoderServlet.doPost(request,response);
        // OK verify response - its JSON so use jackson to decode
        String jsonString = outputStream.data.toString();
        assertNotNull(jsonString);

        Map<String,Object> json = objectMapper.readValue(jsonString,Map.class);
        // Assert success present and has a value of failure  and bad request
        assertTrue("Success field missing in " + jsonString, json.containsKey("success"));
        assertTrue("Success should be false " + jsonString, !((Boolean) json.get("success")));
        assertEquals("Should return Bad Request 400 " + jsonString, 400, json.get("errorCode"));

    }

    /**
     *  This tests the check that we get insufficient storage error
     */
    @Test
    public void testPlainPostBadTempDir()  throws Exception{
        String currentTempDir = System.getProperty(Base64EncoderServlet.JAVA_IO_TMPDIR);

        // Set a random non-existing tmp dir
        System.setProperty(Base64EncoderServlet.JAVA_IO_TMPDIR, "/thisdoesnotexists");
        try {
            HttpServletRequest request = mock(HttpServletRequest.class);
            HttpServletResponse response = mock(HttpServletResponse.class);
            StubServletOutputStream outputStream = new StubServletOutputStream();
            ServletConfig servletConfig = mock(ServletConfig.class);
            ServletContext servletContext = mock(ServletContext.class);
            when(servletConfig.getServletContext()).thenReturn(servletContext);
            when(servletConfig.getServletContext().getInitParameter("maxMemoryFileSize")).thenReturn("128000");
            when(servletConfig.getServletContext().getInitParameter("maxFileSize")).thenReturn("256000");
            when(servletConfig.getServletContext().getInitParameter("maxNumberFiles")).thenReturn("2");
            when(request.getMethod()).thenReturn("post");
            when(request.getContentType()).thenReturn("application/json");


            Base64EncoderServlet encoderServlet = new Base64EncoderServlet();
            encoderServlet.init(servletConfig); // Mock

            // Create data block
            byte[] inputStreamBytes = createDataBlock(200);

            // Setup the input and output streams
            when(request.getInputStream()).thenReturn(createServletInputStream(inputStreamBytes));
            when(response.getOutputStream()).thenReturn( outputStream );
            // Make call
            encoderServlet.doPost(request,response);
            // OK verify response - its JSON so use jackson to decode
            String jsonString = outputStream.data.toString();
            assertNotNull(jsonString);

            Map<String,Object> json = objectMapper.readValue(jsonString,Map.class);
            // Assert success present and has a value of failure  and bad request
            assertTrue("Success field missing in " + jsonString, json.containsKey("success"));
            assertTrue("Success should be false " + jsonString, !((Boolean) json.get("success")));
            assertTrue("Error Code Missing  " + jsonString,json.containsKey("errorCode"));
            assertTrue("Error Message  Missing  " + jsonString,json.containsKey("msg"));
            assertNotNull("Error Message  Null  " + jsonString,json.get("msg"));

            Integer errorCode = (Integer)json.get("errorCode");
            assertTrue(String.format("Expected Error Code %d got %d", Base64EncoderServlet.INSUFFICIENT_STORAGE, errorCode.intValue()), errorCode.intValue() == Base64EncoderServlet.INSUFFICIENT_STORAGE);

        }
        finally {
            // Restore just in case
            System.setProperty(Base64EncoderServlet.JAVA_IO_TMPDIR, currentTempDir);
        }

    }


    /**
     * Valid Test Case - multipart but one file
     */
    @Test
    public void testMultipartPost()  throws Exception{
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
        StubServletOutputStream outputStream = new StubServletOutputStream();
        ServletConfig servletConfig = mock(ServletConfig.class);
        ServletContext servletContext = mock(ServletContext.class);
        when(servletConfig.getServletContext()).thenReturn(servletContext);
        when(servletConfig.getServletContext().getInitParameter("maxMemoryFileSize")).thenReturn("1000");
        when(servletConfig.getServletContext().getInitParameter("maxFileSize")).thenReturn("64000");
        when(servletConfig.getServletContext().getInitParameter("maxNumberFiles")).thenReturn("2");
        when(request.getMethod()).thenReturn("post");
        when(request.getContentType()).thenReturn("multipart/form-data, boundary=" + BOUNDARY_CODE);


        Base64EncoderServlet encoderServlet = new Base64EncoderServlet();
        encoderServlet.init(servletConfig); // Mock

        // Create data block
        byte[] inputStreamBytes = generateMultipartData(1,128); // Values less than above - 1 file example
        // Setup the input and output streams
        when(request.getInputStream()).thenReturn(createServletInputStream(inputStreamBytes));
        when(response.getOutputStream()).thenReturn( outputStream );
        // Make call

        encoderServlet.doPost(request,response);
        // OK verify response - its JSON so use jackson to decode
        String jsonString = outputStream.data.toString();
        assertNotNull(jsonString);

        Map<String,Object> json = objectMapper.readValue(jsonString,Map.class);
        // Assert success present and has a value of success
        assertTrue("Success field missing in " + jsonString, json.containsKey("success"));
        assertTrue("Success should be true " + jsonString, ((Boolean) json.get("success")));

        List<Map<String,Object>> files = (List<Map<String, Object>>) json.get("files");
        assertNotNull("No Files encoded!!!",files);
        assertTrue("Expected 1 files, but got "+files.size(),files.size()==1);
        // dont need to verify encoded contents since that is done in testPlainPost()
    }

    /**
     * Multifile test scenario - valid
     */
    @Test
    public void testMultipartPostMultipleFile()  throws Exception{
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
        StubServletOutputStream outputStream = new StubServletOutputStream();
        ServletConfig servletConfig = mock(ServletConfig.class);
        ServletContext servletContext = mock(ServletContext.class);
        when(servletConfig.getServletContext()).thenReturn(servletContext);
        when(servletConfig.getServletContext().getInitParameter("maxMemoryFileSize")).thenReturn("1000");
        when(servletConfig.getServletContext().getInitParameter("maxFileSize")).thenReturn("64000");
        when(servletConfig.getServletContext().getInitParameter("maxNumberFiles")).thenReturn("5");
        when(request.getMethod()).thenReturn("post");
        when(request.getContentType()).thenReturn("multipart/form-data, boundary=" + BOUNDARY_CODE);


        Base64EncoderServlet encoderServlet = new Base64EncoderServlet();
        encoderServlet.init(servletConfig); // Mock

        // Create data block
        byte[] inputStreamBytes = generateMultipartData(3,128); // Values less than above

        // Setup the input and output streams
        when(request.getInputStream()).thenReturn(createServletInputStream(inputStreamBytes));
        when(response.getOutputStream()).thenReturn( outputStream );
        // Make call

        encoderServlet.doPost(request,response);
        // OK verify response - its JSON so use jackson to decode
        String jsonString = outputStream.data.toString();
        assertNotNull(jsonString);

        Map<String,Object> json = objectMapper.readValue(jsonString,Map.class);
        // Assert success present and has a value of success
        assertTrue("Success field missing in " + jsonString, json.containsKey("success"));
        assertTrue("Success should be true " + jsonString, ((Boolean) json.get("success")));

        List<Map<String,Object>> files = (List<Map<String, Object>>) json.get("files");
        assertNotNull("No Files encoded!!!",files);
        assertTrue("Expected 3 files, but got "+files.size(),files.size()==3);
        // dont need to verify encoded contents since that is done in testPlainPost()

    }

    /**
     * Scenario where we send more files than configured
     */
    @Test
    public void testMultipartPostTooManyFiles()  throws Exception{
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
        StubServletOutputStream outputStream = new StubServletOutputStream();
        ServletConfig servletConfig = mock(ServletConfig.class);
        ServletContext servletContext = mock(ServletContext.class);
        when(servletConfig.getServletContext()).thenReturn(servletContext);
        when(servletConfig.getServletContext().getInitParameter("maxMemoryFileSize")).thenReturn("1000");
        when(servletConfig.getServletContext().getInitParameter("maxFileSize")).thenReturn("64000");
        when(servletConfig.getServletContext().getInitParameter("maxNumberFiles")).thenReturn("2");
        when(request.getMethod()).thenReturn("post");
        when(request.getContentType()).thenReturn("multipart/form-data, boundary=" + BOUNDARY_CODE);


        Base64EncoderServlet encoderServlet = new Base64EncoderServlet();
        encoderServlet.init(servletConfig); // Mock

        // Create data block
        byte[] inputStreamBytes = generateMultipartData(3,128); // Values less than above

        // Setup the input and output streams
        when(request.getInputStream()).thenReturn(createServletInputStream(inputStreamBytes));
        when(response.getOutputStream()).thenReturn( outputStream );
        // Make call

        encoderServlet.doPost(request,response);
        // OK verify response - its JSON so use jackson to decode
        String jsonString = outputStream.data.toString();
        assertNotNull(jsonString);

        Map<String,Object> json = objectMapper.readValue(jsonString,Map.class);
        // Assert success present and has a value of success
        assertTrue("Success field missing in " + jsonString, json.containsKey("success"));
        assertTrue("Success should be false " + jsonString, !((Boolean) json.get("success")));
        assertTrue("Error Code Missing  " + jsonString,json.containsKey("errorCode"));
        assertTrue("Error Message  Missing  " + jsonString,json.containsKey("msg"));
        assertNotNull("Error Message  Null  " + jsonString,json.get("msg"));

        Integer errorCode = (Integer)json.get("errorCode");
        assertTrue(String.format("Expected Error Code %d got %d",Base64EncoderServlet.TOO_MANY_FILES,errorCode.intValue()), errorCode.intValue()==Base64EncoderServlet.TOO_MANY_FILES);
    }


    /**
     * Should get not content error
     */
    @Test
    public void testMultipartPostNoContent()  throws Exception{
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
        StubServletOutputStream outputStream = new StubServletOutputStream();
        ServletConfig servletConfig = mock(ServletConfig.class);
        ServletContext servletContext = mock(ServletContext.class);
        when(servletConfig.getServletContext()).thenReturn(servletContext);
        when(servletConfig.getServletContext().getInitParameter("maxMemoryFileSize")).thenReturn("1000");
        when(servletConfig.getServletContext().getInitParameter("maxFileSize")).thenReturn("64000");
        when(servletConfig.getServletContext().getInitParameter("maxNumberFiles")).thenReturn("2");
        when(request.getMethod()).thenReturn("post");
        when(request.getContentType()).thenReturn("multipart/form-data, boundary=" + BOUNDARY_CODE);


        Base64EncoderServlet encoderServlet = new Base64EncoderServlet();
        encoderServlet.init(servletConfig); // Mock


        // Setup the input and output streams       (input being null)
        when(request.getInputStream()).thenReturn(createServletInputStream(null));
        when(response.getOutputStream()).thenReturn( outputStream );
        // Make call

        encoderServlet.doPost(request,response);
        // OK verify response - its JSON so use jackson to decode
        String jsonString = outputStream.data.toString();
        assertNotNull(jsonString);

        Map<String,Object> json = objectMapper.readValue(jsonString,Map.class);
        // Assert success present and has a value of success
        assertTrue("Success field missing in " + jsonString, json.containsKey("success"));
        assertTrue("Success should be false " + jsonString, !((Boolean) json.get("success")));
        assertTrue("Error Code Missing  " + jsonString,json.containsKey("errorCode"));
        assertTrue("Error Message  Missing  " + jsonString,json.containsKey("msg"));
        assertNotNull("Error Message  Null  " + jsonString,json.get("msg"));

        Integer errorCode = (Integer)json.get("errorCode");
        assertTrue(String.format("Expected Error Code %d got %d",Base64EncoderServlet.BAD_REQUEST,errorCode.intValue()), errorCode.intValue()==Base64EncoderServlet.BAD_REQUEST);

    }

    /**
     *  Test when we have too much data - define mag file size is 32000 and send 64000 to it
     */
    @Test
    public void testMultipartPostTooMuchContent()  throws Exception{
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
        StubServletOutputStream outputStream = new StubServletOutputStream();
        ServletConfig servletConfig = mock(ServletConfig.class);
        ServletContext servletContext = mock(ServletContext.class);
        when(servletConfig.getServletContext()).thenReturn(servletContext);
        when(servletConfig.getServletContext().getInitParameter("maxMemoryFileSize")).thenReturn("1000");
        when(servletConfig.getServletContext().getInitParameter("maxFileSize")).thenReturn("32000");
        when(servletConfig.getServletContext().getInitParameter("maxNumberFiles")).thenReturn("2");
        when(request.getMethod()).thenReturn("post");
        when(request.getContentType()).thenReturn("multipart/form-data, boundary=" + BOUNDARY_CODE);


        Base64EncoderServlet encoderServlet = new Base64EncoderServlet();
        encoderServlet.init(servletConfig); // Mock

        // Create data block
        byte[] inputStreamBytes = generateMultipartData(1,64000); // Values less than above

        // Setup the input and output streams       (input being null)
        when(request.getInputStream()).thenReturn(createServletInputStream(inputStreamBytes));
        when(response.getOutputStream()).thenReturn( outputStream );
        // Make call

        encoderServlet.doPost(request,response);
        // OK verify response - its JSON so use jackson to decode
        String jsonString = outputStream.data.toString();
        assertNotNull(jsonString);

        Map<String,Object> json = objectMapper.readValue(jsonString,Map.class);
        // Assert success present and has a value of success
        assertTrue("Success field missing in " + jsonString, json.containsKey("success"));
        assertTrue("Success should be false " + jsonString, !((Boolean) json.get("success")));
        assertTrue("Error Code Missing  " + jsonString,json.containsKey("errorCode"));
        assertTrue("Error Message  Missing  " + jsonString,json.containsKey("msg"));
        assertNotNull("Error Message  Null  " + jsonString,json.get("msg"));

        Integer errorCode = (Integer)json.get("errorCode");
        assertTrue(String.format("Expected Error Code %d got %d",Base64EncoderServlet.REQUEST_ENTITY_TOO_LARGE,errorCode.intValue()), errorCode.intValue()==Base64EncoderServlet.REQUEST_ENTITY_TOO_LARGE);

    }

    /**
     * We do a vanilla post - we should get a 'success' response
     */
    @Test
    public void testAgentPostIE() throws Exception {
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
        StubServletOutputStream outputStream = new StubServletOutputStream();
        ServletConfig servletConfig = mock(ServletConfig.class);
        ServletContext servletContext = mock(ServletContext.class);
        when(servletConfig.getServletContext()).thenReturn(servletContext);
        when(servletConfig.getServletContext().getInitParameter("maxMemoryFileSize")).thenReturn("128000");
        when(servletConfig.getServletContext().getInitParameter("maxFileSize")).thenReturn("256000");
        when(servletConfig.getServletContext().getInitParameter("maxNumberFiles")).thenReturn("2");
        when(request.getMethod()).thenReturn("post");
        when(request.getHeader("user-agent")).thenReturn("Mozilla/5.0 (Windows; U; MSIE 9.0; WIndows NT 9.0; en-US)");


        Base64EncoderServlet encoderServlet = new Base64EncoderServlet();
        encoderServlet.init(servletConfig); // Mock

        // Create data block
        byte[] inputStreamBytes = createDataBlock(128);

        // Setup the input and output streams
        when(request.getInputStream()).thenReturn(createServletInputStream(inputStreamBytes));
        when(response.getOutputStream()).thenReturn( outputStream );
        // Make call
        encoderServlet.doPost(request,response);
        // Verify agent type
        verify(response).setContentType("text/html");

        // OK verify response - its JSON so use jackson to decode
        String jsonString = outputStream.data.toString();
        assertNotNull(jsonString);

        Map<String,Object> json = objectMapper.readValue(jsonString,Map.class);
        // Assert success present and has a value of success
        assertTrue("Success field missing in " + jsonString, json.containsKey("success"));
        assertTrue("Success should be true " + jsonString, ((Boolean) json.get("success")));

        List<Map<String,Object>> files = (List<Map<String, Object>>) json.get("files");
        assertNotNull("No Files encoded!!!",files);
        assertNotNull("Files enmpty!!!",files.size()>0);
        byte[] decoded = decodeBase64((String)files.get(0).get("base64Data"));
        // Now we need to verify what we sent was encoded correctly
        assertArrayEquals("Byte Mismatch", inputStreamBytes,decoded);

    }



    /**
     * This creates a datablock of a certain size
     *
     * @param numberofcharacters
     * @return
     */
    private byte[] createDataBlock(int numberofcharacters) {
        byte[] content = new byte[numberofcharacters];
        byte[] chars = {'0','1','2','3','4','5','6','7','8','9'};
        for(int i=0; i<numberofcharacters; i++){

            content[i]=chars[i%10];
        }
        return content;
    }


    /**
     * This static method allows us to create the actual payload
     *
     * @param bytes
     * @return
     * @throws Exception
     */
    public static ServletInputStream createServletInputStream( byte[] bytes) throws Exception {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        if( bytes != null ){
            baos.write(bytes);
        }

        final InputStream bais = new ByteArrayInputStream(baos.toByteArray());

        return new ServletInputStream() {

            @Override
            public int read() throws IOException {
                return bais.read();
            }
        };
    }


    public class StubServletOutputStream extends ServletOutputStream {
        public ByteArrayOutputStream data = new ByteArrayOutputStream();

        public void write(int i) throws IOException {
            data.write(i);
        }
    }


    /**
     * This method generated a payload representing n files of a given length
     * in format compliant with http://www.faqs.org/rfcs/rfc1867.html
     *
     * @param numberOfFiles
     * @param numCharacters
     * @return
     */
    private byte[] generateMultipartData(int numberOfFiles, int numCharacters){
        StringBuffer content = new StringBuffer();

        for(int i=0; i<numberOfFiles; i++){
            String fileSection = MULTIPART_FILE;
            fileSection = fileSection.replace("$$FILENAME$$","file-"+(i+1)+".txt");
            fileSection = fileSection.replace("$$N$$","file-"+(i+1)+".txt");
            StringBuffer payload = new StringBuffer();

            for( int j=0; j<numCharacters; j++) {
                payload.append('A');
            }
            fileSection = fileSection.replace("$$PAYLOAD$$",payload.toString());
            content.append(fileSection);
        }
        String finalPayload = MULTIPART_FORM.replace("$$CONTENT$$",content.toString());
        return finalPayload.getBytes();
    }

    /**
     * Takes base 64 stream and decodes it;
     *
     * @param base64
     * @return
     */
    private byte[] decodeBase64(String base64) {
        if( base64 == null ){
            return null;
        }
        byte[] result = Base64.decodeBase64(base64.getBytes());
        return result;

    }



}
