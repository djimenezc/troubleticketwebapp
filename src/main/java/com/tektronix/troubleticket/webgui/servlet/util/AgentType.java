package com.tektronix.troubleticket.webgui.servlet.util;

/**
 * Copyright (c) 2013 Tekcomms
 * <p/>
 * Trouble Ticket Integration Project
 * <p/>
 * User: gleeson
 * Date: 28/11/13
 * Time: 17:08
 * <p/>
 * This Enum defines agent type
 */
public enum AgentType {
    Chrome,Firefox,InternetExplorer,Unknown
}
