package com.tektronix.troubleticket.webgui.servlet.util;

/**
 * Copyright (c) 2013 Tekcomms
 * <p/>
 * Trouble Ticket Integration Project
 * <p/>
 * User: gleeson
 * Date: 28/11/13
 * Time: 16:10
 * <p/>
 * This Class this class does browser recognition
 */
public class ClientAgent {

    private String agent;
    private String agentVersion ="unknown";
    private AgentType agentType=AgentType.Unknown;

    public  ClientAgent(String agent){
      this.agent = agent;
      recognize(agent);
    }


    private void recognize(String userAgent) {
        if( userAgent == null){
            agentType=AgentType.Unknown;
            agentVersion="Unknown";
        }
        else if(userAgent.contains("Chrome")){ //checking if Chrome
            String substring=userAgent.substring(userAgent.indexOf("Chrome")).split(" ")[0];
            agentType=AgentType.Chrome;
            agentVersion =substring.split("/")[1];
        }
        else if(userAgent.contains("Firefox")){  //Checking if Firefox
            String substring=userAgent.substring(userAgent.indexOf("Firefox")).split(" ")[0];
            agentType=AgentType.Firefox;
            agentVersion =substring.split("/")[1];
        }
        else if(userAgent.contains("MSIE")){ //Checking if Internet Explorer
            String substring=userAgent.substring(userAgent.indexOf("MSIE")).split(";")[0];
            agentType=AgentType.InternetExplorer;
            agentVersion =substring.split(" ")[1];
        }
    }


    public String getAgent() {
        return agent;
    }

    public String getAgentVersion() {
        return agentVersion;
    }

    public AgentType getAgentType() {
        return agentType;
    }
}
