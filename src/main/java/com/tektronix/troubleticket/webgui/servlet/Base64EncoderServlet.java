package com.tektronix.troubleticket.webgui.servlet;

import com.tektronix.troubleticket.webgui.servlet.util.AgentType;
import com.tektronix.troubleticket.webgui.servlet.util.ClientAgent;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadBase;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.SerializationConfig;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Copyright (c) 2013 Tekcomms.
 * <p/>
 * Trouble Ticket Integration Project
 * <p/>
 * User: gleeson
 * Date: 20/11/13
 * Time: 14:24
 * <p/>
 * This Class simple servlet that takes a post request and returns it as a JSON snippet
 */
public class Base64EncoderServlet extends HttpServlet {
    public static final int BYTES_IN_MEGABYTE = 1000000;
    public static final int BYTES_IN_KILOBYTE = 1000;
    public static final String MAX_FILE_SIZE_PARAMETER = "maxFileSize";
    public static final String MAX_MEMORY_SIZE_PARAMETER = "maxMemoryFileSize";
    public static final String MAX_FILES_PARAMETER = "maxNumberFiles";
    public static final String JAVA_IO_TMPDIR = "java.io.tmpdir";
    private static final long serialVersionUID = -3174001949871218321L;

    private transient Logger log = Logger.getLogger(this.getClass());

    private static final String ERROR_CODE_FIELD = "errorCode";
    private static final String FILENAME_FIELD = "filename";
    private static final String MIME_FIELD = "mimetype";
    private static final String BINARY_SIZE_FIELD = "binarySize";
    private static final String BASE_64_DATA_FIELD = "base64Data";
    private static final String FILES_FIELD = "files";
    private static final String SUCCESS_FIELD = "success";
    private static final String MESSAGE_FIELD = "msg";
    public static final String TEXT_HTML = "text/html"; // IE hack to prevent it prompt for download

    public static final int SUCCESS = 200;
    public static final int TOO_MANY_FILES = 406;
    public static final int REQUEST_ENTITY_TOO_LARGE = 413;
    public static final int INSUFFICIENT_STORAGE = 507;
    public static final int BAD_REQUEST = 400;
    public static final int THREE = 3;
    public static final int FOUR = 4;


    private static final int DEFAULT_MAX_NUMBER_FILES = 5;
    private static final int DEFAULT_MAX_FILE_SIZE = 10000000;
    private static final int DEFAULT_MAX_MEMORY_SIZE = 128000;

    private static boolean dumpedConfig = false;


    /**
     *
     */
    public Base64EncoderServlet() {
        super();
    }


    /**
     *  This method takes a post body and encodes it into base64 string
     *
     * @param req
     * @param resp
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        long started = System.currentTimeMillis();
        long numberOfFiles=0;
        String returnContentType= TEXT_HTML;

        try {
            String tempFileDirectory = System.getProperty(JAVA_IO_TMPDIR);
            boolean isMultipart = ServletFileUpload.isMultipartContent(req);
            log.debug("POST CALLED ... multipart = " + isMultipart);
            ClientAgent agent = new ClientAgent(req.getHeader("user-agent"));
            if( agent.getAgentType()== AgentType.InternetExplorer){
                returnContentType = TEXT_HTML;
            }
            // Set return type
            resp.setContentType(returnContentType);
            long maxMemoryFileSize= getLongServletParameter(MAX_MEMORY_SIZE_PARAMETER,DEFAULT_MAX_MEMORY_SIZE);
            long maxDiskFileSize= getLongServletParameter(MAX_FILE_SIZE_PARAMETER,DEFAULT_MAX_FILE_SIZE);
            long maxNumberOfFiles= getLongServletParameter(MAX_FILES_PARAMETER,DEFAULT_MAX_NUMBER_FILES);

            // We only want to dump this information once because it would be tedious otherwise
            if(!dumpedConfig){
                log.info(String.format("Max Memory Size [%d] Max Disk Size [%d] Max Files [%d]",maxMemoryFileSize,maxDiskFileSize,maxNumberOfFiles));
                dumpedConfig=true;
            }

            // Sanity Checking - can we write temporary files
            File tempDir = new File(tempFileDirectory);
            if (!tempDir.exists() || !tempDir.canWrite()) {
                final String message = "Temporary directory '" + tempFileDirectory + "' either does not exist or cannot be written to.";
                onError(resp, INSUFFICIENT_STORAGE, message,null,false);
                return;
            }

            try {
                // File upload
                if (isMultipart) {
                    // Create a factory for disk-based file items
                    DiskFileItemFactory factory = new DiskFileItemFactory();

                    // Configure a repository (to ensure a secure temp location is used)
                    // Set factory constraints
                    factory.setSizeThreshold((int)maxMemoryFileSize );
                    factory.setRepository(tempDir);

                    // Create a new file upload handler
                    ServletFileUpload upload = new ServletFileUpload(factory);

                    // Set overall request size constraint
                    upload.setSizeMax(maxDiskFileSize);

                    // Parse the request
                    try{
                        List<FileItem> items = upload.parseRequest(req);
                        numberOfFiles = items.size();
                        if( items.size() >maxNumberOfFiles ){
                            onError(resp,TOO_MANY_FILES,"The maximum number of files that can be accepted is "+maxNumberOfFiles,null,true);
                        }
                        else {
                            onMultipartSubmission(resp, items);
                        }
                    }
                    catch(FileUploadBase.FileSizeLimitExceededException fex){
                        onError(resp,REQUEST_ENTITY_TOO_LARGE,fex.getMessage(),fex,true);
                    }
                    catch(FileUploadBase.SizeLimitExceededException sle){
                        onError(resp,REQUEST_ENTITY_TOO_LARGE,sle.getMessage(),sle,true);
                    }
                } else {
                    // Straight Post
                    numberOfFiles=1;
                    onNonMultipartSubmission(req,resp);
                }
            }
            catch(Exception e){
                onError(resp, BAD_REQUEST, e.getMessage(),e,false);
            }
        }
        finally {
            long elapsed = System.currentTimeMillis()-started;
            log.info(String.format("Base64Encoder Encoding Elapsed[%d] ms for Files#[%d] Return Content Type [%s]",elapsed,numberOfFiles,returnContentType));
        }


    }


    /**
     * @param resp
     * @param files
     * @throws IOException
     */
    private void onMultipartSubmission(HttpServletResponse resp, List<FileItem> files) throws IOException {
        Map<String,Object> returnMessage = new HashMap<String, Object>();
        returnMessage.put(SUCCESS_FIELD, true);
        ArrayList<Map<String,Object>> attachments = new ArrayList<Map<String, Object>>();

        for (FileItem file : files) {
            log.debug(String.format("Type [%s] Name [%s] Mime [%s] inMem [%s] %n", (file.isFormField() ? "Form" : "File"), file.getName(), file.getContentType(), file.isInMemory()));
            Map<String,Object> fileRecord = new HashMap<String, Object>();
            fileRecord.put(FILENAME_FIELD,file.getName()==null?"File"+System.currentTimeMillis()+".txt":file.getName());
            fileRecord.put(MIME_FIELD,file.getContentType()==null?"text/plain":file.getContentType());
            String encodedData = base64Encode(file.getInputStream());
            fileRecord.put(BASE_64_DATA_FIELD, encodedData);
            fileRecord.put(BINARY_SIZE_FIELD,file.getSize());
            attachments.add(fileRecord);
        }

        if( attachments.size() > 0){
            returnMessage.put(FILES_FIELD, attachments);
            writeJSONResponse(resp, SUCCESS, returnMessage,true);
        }
        else {
            onError(resp,BAD_REQUEST,"No Payload Defined",null,true);
        }
    }



    /**
     * This method is basically used to write an error response
     *
     * @param resp
     * @param code
     * @param message
     */
    private void onError(HttpServletResponse resp, int code, String message,Exception e,boolean multipart) throws IOException {
        log.error("Error code " + code + " " + message, e);
        Map<String,Object> responseObject = new HashMap<String, Object>();
        responseObject.put(SUCCESS_FIELD, (code == SUCCESS));
        responseObject.put(ERROR_CODE_FIELD, code);
        responseObject.put(MESSAGE_FIELD, message);
        writeJSONResponse(resp,code,responseObject,multipart);
    }


    /**
     * This method is encode the input stream
     */
    private void onNonMultipartSubmission(HttpServletRequest req,HttpServletResponse resp) throws IOException {
        Map<String,Object>returnObject = new HashMap<String, Object>();
        ArrayList<Map<String,Object>> attachments = new ArrayList<Map<String, Object>>();
        Map<String,Object> fileRecord = new HashMap<String, Object>();

        // Will return empty string if stream is null
        String encodedData =  base64Encode(req.getInputStream());
        long encodedLength=  encodedData.getBytes(Charset.defaultCharset() ).length;
        if( encodedData == null || encodedData.length()==0){
           onError(resp,BAD_REQUEST,"No Payload Defined",null,false);
        }
        else {
            fileRecord.put(FILENAME_FIELD,"File"+System.currentTimeMillis());
            fileRecord.put(MIME_FIELD, req.getContentType());
            fileRecord.put(BASE_64_DATA_FIELD,encodedData);
            fileRecord.put(BINARY_SIZE_FIELD,(encodedLength/FOUR)*THREE);

            attachments.add(fileRecord);
            returnObject.put(FILES_FIELD,attachments);
            returnObject.put(SUCCESS_FIELD, true);
            writeJSONResponse(resp, SUCCESS, returnObject,false);
        }

    }


    /**
     * Actual Writing to Output Occurs here
     *
     * @param resp
     * @param responseObject
     * @throws IOException
     */
    private void writeJSONResponse(HttpServletResponse resp,int statusCode,Map<String,Object> responseObject,boolean multipart) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.setSerializationInclusion(JsonSerialize.Inclusion.NON_NULL);
        objectMapper.enable(SerializationConfig.Feature.INDENT_OUTPUT);

        resp.setStatus(statusCode);
        responseObject.put(ERROR_CODE_FIELD,statusCode);
        String json = objectMapper.writeValueAsString(responseObject);
        String errorMessage = (String)(responseObject.get(MESSAGE_FIELD)==null?"": responseObject.get(MESSAGE_FIELD));
        String msg;

        if( json.length() > BYTES_IN_MEGABYTE){
            double len=(double)json.length();
            msg = String.format("Code [%d] Message [%s] Length [%01.1f Mb] Multipart[%s]",statusCode, errorMessage,  (len / ((double) BYTES_IN_MEGABYTE)),multipart);
        }
        else if( json.length() > BYTES_IN_KILOBYTE) {
            msg = String.format("Code [%d] Message [%s] Length [%d Kb] Multipart[%s]",statusCode,responseObject.get(MESSAGE_FIELD),(json.length()/ BYTES_IN_KILOBYTE),multipart);
        }
        else {
            msg = String.format("Code [%d] Message [%s] Length [%d Bytes] Multipart[%s]",statusCode,responseObject.get(MESSAGE_FIELD),json.length(),multipart);
        }
        log.info(msg);
        log.debug(json);
        resp.getOutputStream().write(json.getBytes(Charset.defaultCharset()));
        resp.getOutputStream().flush();
    }

    /**
     * Encodes input stream into base64
     *
     * @param inputStream
     * @return
     */
    private String base64Encode(InputStream inputStream) throws IOException {
        String result=null;

        if( inputStream == null) {
            return "";
        }

        result = new String(Base64.encodeBase64(IOUtils.toByteArray(inputStream)),Charset.defaultCharset());
        return result;
    }




    /**
     * There seems to be an issue which means init parameters are only set on the actual
     * request so you cannot read them when servlet started.
     */
    private long getLongServletParameter(String name,long defaultValue){
        if(super.getServletContext()== null) {
            return  defaultValue;
        }
        String v = super.getServletContext().getInitParameter(name);
        long result;
        try {
            result = Long.parseLong(v);
        } catch (Exception ex) {
            result=defaultValue;
        }
        return result;
    }
}
