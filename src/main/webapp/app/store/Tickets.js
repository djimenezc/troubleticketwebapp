/**
 * The store class for the {@link TroubleTicket.model.ticket.Ticket Ticket} model
 */
Ext.define('TroubleTicket.store.Tickets', {
    extend: 'Ext.data.Store',
    model: 'TroubleTicket.model.ticket.Ticket',
    requires: [
        'Ext.ux.data.reader.DynamicJson',
        'Ext.overrides.data.proxy.ServerOverride',
        'TroubleTicket.config.model.ticket'
    ],
    /**
     * We override the constructor to setup the proxy if it is not done yet. The setup is in the configuration:
     * app/config/store/tickets.js
     * @constructor
     */
    constructor: function () {
        var res = this.callParent(arguments),
            proxyCfg;
        if (!this.getProxy()) {
            proxyCfg = Ext.ns('TroubleTicket.config.model.tickets.proxy');
            if (!Ext.Object.isEmpty(proxyCfg)) {
                this.setProxy(proxyCfg);
            }
        }
        return res;
    },
    
    listeners: {
        'load' :  function(store,records,options) {
            TroubleTicket.app.viewport.setLoading(false);
        }
    }
});