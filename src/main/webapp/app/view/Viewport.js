/**
 * This is the viewport class for the TroubleTicket application. This class is used to hold the application layout.
 * @class TroubleTicket.view.Viewport
 */
Ext.define('TroubleTicket.view.Viewport', {
    extend: 'Ext.container.Viewport',
    requires: [
        'Ext.layout.container.Fit',
        'TroubleTicket.view.Main'
    ],
    layout: {
        type: 'fit'
    }
});
