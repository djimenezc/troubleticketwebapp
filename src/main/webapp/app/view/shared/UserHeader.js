Ext.define("TroubleTicket.view.shared.UserHeader", {
    extend: 'Ext.panel.Panel',
    alias: 'widget.userHeader',
    ui: 'transparent-panel',
    cls: 'tt-user-header-ctr'
});