/**
 * The breadcrumb component for the application. Not really used.
 */
Ext.define("TroubleTicket.view.shared.AppBreadcrumb", {
    extend: 'Ext.container.Container',
    alias: 'widget.appBreadcrumb',
    layout: 'fit',
    cls: 'tt-app-breadrumb',
    autoRender: true,
    height: 40,
    /**
     * @cfg {String} itemTpl
     * The template for one item in the path
     */
    itemTpl: '<span class="{cls} tt-app-breadcrumb-path" data-token="{token}">'
        + '<a href="javascript: void(0);">{label}</a>'
        + '</span>',
    html: '',
    init: function () {
        "use strict";
        if (!(this.itemTpl instanceof Ext.Template)) {
            this.itemTpl = new Ext.Template(this.itemTpl);
        }
    },
    afterRender: function () {
        var res = this.callParent(arguments);
        this.el.on('click', this.onPathClick, this, {
            delegate: '[data-token]'
        });
        return res;
    },
    /**
     * Sets a path with tokens
     * @param {Object/Array} paths It is an array with objects or just one object with the following properties:
     * - label: the string to display
     * - token: the token for the Ext.Router.redirect
     * - cls: extra css class for the item
     * {
     *  label: 'Home',
     *  token: '/'
     * }
     */
    setPath: function (paths) {
        "use strict";
        var html = '';
        if (Ext.isObject(paths)) {
            paths = [paths];
        }
        Ext.each(paths, function (path) {
            html += this.createItemHtml(path);
        }, this);
        this.update(html);
    },
    /**
     * Create the html of an element applying the given cfg to the template
     * @param {Object} cfg
     * @return {HTML}
     */
    createItemHtml: function (cfg) {
        "use strict";
        var tpl = new Ext.Template(this.itemTpl);
        return tpl.apply(cfg);
    },
    /**
     * Clear the body html
     */
    clearPath: function () {
        "use strict";
        this.update('');
    },
    /**
     * Event handler for the click event on a path element
     * @private
     */
    onPathClick: function (ev, el) {
        "use strict";
        var token = el.getAttribute('data-token');
        if (token) {
            TroubleTicket.getApplication().getViewport().setLoading(true);
            Ext.Router.redirect(token);
        }
    }
});