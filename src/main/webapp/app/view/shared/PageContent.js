/**
 * This class to hold the content of the page. It will load the {@link TroubleTicket.view.shared.AppContent appContent}
 * This container's with is the window with.
 */
Ext.define("TroubleTicket.view.shared.PageContent", {
    extend: 'Ext.panel.Panel',
    alias: 'widget.pageContent',
    layout: {
        type: 'hbox',
        align: 'stretch'
    },
    defaults: {
        flex: 1
    },
    requires: [
        'TroubleTicket.view.shared.AppContent'
    ],
    items: [{
        xtype: 'appContent'
    }],
    cls: 'tt-page-content-ctr'
});