/**
 * This is the window wide footer of the app. It will load the {@link TroubleTicket.view.shared.AppFooter appFooter}.
 */
Ext.define("TroubleTicket.view.shared.PageFooter", {
    extend: 'Ext.panel.Panel',
    alias: 'widget.pageFooter',
    requires: [
        'TroubleTicket.view.shared.AppFooter'
    ],
    items: [{
        xtype: 'appFooter'
    }],
    cls: 'tt-page-footer-ctr'
});