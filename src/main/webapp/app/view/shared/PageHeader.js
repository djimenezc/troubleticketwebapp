/**
 * This is the header container of the page. The width of this container equals the window width. It will load the
 * {@link TroubleTicket.view.shared.AppHeader appHeader}
 */
Ext.define("TroubleTicket.view.shared.PageHeader", {
    extend: 'Ext.panel.Panel',
    alias: 'widget.pageHeader',
    layout: 'hbox',
    align: 'stretch',
    requires: [
        'TroubleTicket.view.shared.AppHeader'
    ],
    items: [{
        xtype: 'appHeader'
    }],
    cls: 'tt-page-header-ctr'
});