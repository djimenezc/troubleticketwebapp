/**
 * This is the title container of the application. The title method has been overriden to update the body.
 */
Ext.define("TroubleTicket.view.shared.AppTitle", {
    extend: 'Ext.panel.Panel',
    alias: 'widget.appTitle',
    cls: 'tt-app-title-ctr',
    bodyTpl: '<head class="{cls}"><h1>{title}</h1></head>',
    initComponent: function () {
        if (typeof this.bodyTpl === 'string') {
            this.bodyTpl = new Ext.Template(this.bodyTpl, {
                compiled: true
            });
        }
        this.callParent(arguments);
    },
    /**
     * Set the container's body HTML to the compiled template.
     * @param {String} title
     * @param {String} [cls] The css class for the title.
     */
    setTitle: function (title, cls) {
        "use strict";
        this.bodyTpl.overwrite(this.body, {
            title: title,
            cls: cls || ''
        });
    }
});