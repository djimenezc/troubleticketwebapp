/**
 * This is the body container of the page. It will load the
 * {@link TroubleTicket.view.shared.PageContent pageContent} which holds the application conatiner and will load also
 * the {@link TroubleTicket.view.shared.PageFooter pageFooter}
 */
Ext.define("TroubleTicket.view.shared.PageBody", {
    extend: 'Ext.panel.Panel',
    alias: 'widget.pageBody',
    layout: {
        type: 'hbox',
        align: 'stretch'
    },
    requires: [
        'TroubleTicket.view.shared.PageContent',
        'TroubleTicket.view.shared.PageFooter'
    ],
    items: [{
        xtype: 'pageContent'
    }, {
        xtype: 'pageFooter',
        maxWidth: '100%',
        layout: {
            type: 'vbox',
            align: 'center'
        },
        defaults: {
            width: '100%',
            maxWidth: 960
        }
    }]
});