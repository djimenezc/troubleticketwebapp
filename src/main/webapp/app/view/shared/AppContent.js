/**
 * This is the container element for the application view components. It has one top docked item the breadcrumb.
 */
Ext.define("TroubleTicket.view.shared.AppContent", {
    extend: 'Ext.panel.Panel',
    alias: 'widget.appContent',
    requires: [
        'TroubleTicket.view.shared.AppBreadcrumb'
    ],
    layout: {
        type: 'hbox',
        align: 'stretch'
    },
    defaults: {
        flex: 1
    },
    cls: 'tt-app-content-ctr',
    margin: '0 0 40 0',
    dockedItems: [{
        dock: 'top',
        xtype: 'appBreadcrumb'
    }]
});