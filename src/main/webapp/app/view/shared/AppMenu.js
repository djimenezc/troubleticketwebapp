/**
 * This class is intended to hold the application's menu
 */
Ext.define("TroubleTicket.view.shared.AppMenu", {
    extend: 'Ext.panel.Panel',
    alias: 'widget.appMenu',
    cls: 'tt-app-menu-ctr'
});