/**
 * This is the application wide header for the application.
 */
Ext.define("TroubleTicket.view.shared.AppHeader", {
    extend: 'Ext.panel.Panel',
    alias: 'widget.appHeader',
    layout: {
        type: 'hbox',
        align: 'stretch'
    },
    height: 75,
    requires: [
        'TroubleTicket.view.shared.UserHeader',
        'TroubleTicket.view.shared.AppTitle'
    ],
    items: [{
        xtype: 'panel',
        width: 200,
        cls: 'tt-app-logo-ctr'
    }, {
        xtype: 'appTitle',
        flex: 2,
        cls: 'tt-app-title-ctr'
    }, {
        xtype: 'userHeader',
        flex: 1,
        cls: 'tt-app-user-header-ctr'
    }],
    cls: 'tt-app-header-ctr'
});