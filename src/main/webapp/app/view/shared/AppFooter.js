/**
 * This is the application wide footer for the application.
 */
Ext.define("TroubleTicket.view.shared.AppFooter", {
    extend: 'Ext.panel.Panel',
    alias: 'widget.appFooter',
    layout: 'fit',
    ui: 'transparent-panel',
    height: 75,
    cls: 'tt-app-footer-ctr'
});