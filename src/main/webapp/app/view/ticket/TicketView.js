/**
 * @class TroubleTicket.view.ticket.TicketView
 * This class is a readonly version of the {@link TroubleTicket.view.ticket.TicketForm TicketForm}.
 */
Ext.define("TroubleTicket.view.ticket.TicketView", {
    extend: 'TroubleTicket.view.ticket.TicketForm',
    alias: 'widget.ticketView',
    addPDFPreview: Ext.emptyFn,
    cls: 'cls-ticket-view',
    requires: [
        'TroubleTicket.util.String'
    ],
    items: [{
        xtype: 'fieldset',
        layout: 'column',
        cls: 'tt-custom-fieldset'
    }, {
        xtype: 'fieldset',
        layout: 'anchor',
        defaults: {
            anchor: '100%'
        },
        cls: 'tt-fixed-fieldset',
        items: [{
            xtype: 'fieldcontainer',
            layout: 'hbox',
            fieldLabel: TroubleTicket.util.i18n.translate('formLabels.issueTimePeriod'),
            labelAlign: 'right',
            items: [{
                xtype: 'displayfield',
                value: TroubleTicket.util.i18n.translate('formLabels.from'),
                cls: 'tt-issuetime-from-prefix',
                margins: '0 10 0 10',
                flex: 0
            }, {
                xtype: 'displayfield',
                name: 'startTime',
                cls: 'tt-field-startTime',
                renderer: TroubleTicket.util.Date.formatDateTime,
                flex: 0
            }, {
                xtype: 'displayfield',
                value: TroubleTicket.util.i18n.translate('formLabels.to'),
                cls: 'tt-issuetime-to-prefix',
                margins: '0 10 0 10',
                flex: 0
            }, {
                xtype: 'displayfield',
                name: 'endTime',
                cls: 'tt-field-endTime',
                renderer: TroubleTicket.util.Date.formatDateTime,
                flex: 0
            }]
        }, {
            xtype: 'displayfield',
            name: 'title',
            cls: 'tt-field-title',
            blankText: 'enter issue title',
            fieldLabel: TroubleTicket.util.i18n.translate('formLabels.issueTitle'),
            allowBlank: false,
            maxLength: 255,
            validateOnBlur: true
        }, {
            xtype: 'displayfield',
            name: 'location',
            cls: 'tt-field-location',
            fieldLabel: TroubleTicket.util.i18n.translate('formLabels.issueLocation')
        }, {
            xtype: 'displayfield',
            name: 'status',
            cls: 'tt-field-status',
            fieldLabel: TroubleTicket.util.i18n.translate('formLabels.status'),
            renderer: function (v) {
                return TroubleTicket.util.i18n.translate('status.'+v);
            }
        }, {
            xtype: 'displayfield',
            name: 'description',
            renderer: function (v) {
                "use strict";
                return TroubleTicket.util.String.newLineToBr(v);
            },
            cls: 'tt-field-description',
            fieldLabel: TroubleTicket.util.i18n.translate('formLabels.description'),
            allowBlank: false,
            validateOnBlur: true
        }, {
            xtype: 'fieldcontainer',
            cls: 'tt-comments-fieldctr',
            fieldLabel: TroubleTicket.util.i18n.translate('formLabels.comment'),
            layout: {
                type: 'vbox',
                align: 'stretch'
            },
            hidden: true,
            width: '100%'
        }, {
            xtype: 'hidden',
            cls: 'tt-field-id',
            name: 'id'
        }, {
            xtype: 'hidden',
            allowBlank: false,
            cls: 'tt-pdf-sourceapplication',
            name: 'sourceApplication'
        }, {
            xtype: 'fieldcontainer',
            cls: 'tt-attachments-fieldctr',
            fieldLabel: TroubleTicket.util.i18n.translate('formLabels.attachments'),
            layout: {
                type: 'vbox',
                align: 'stretch'
            },
            width: '100%'
        }]
    }],
    buttons: [{
        text: TroubleTicket.util.i18n.translate('buttons.backToSearch'),
        cls: 'tt-secondary-btn',
        ctrAction: 'cancel'
    }],
    /**
     * @cfg {String} editBtnSelector
     * The component selector to find the cancel button. It is used in {@link #getCancelBtn}.
     */
    editBtnSelector: 'button[cls~=tt-edit-btn]',
    /**
     * Override the second arguments: it is read only in view mode
     * @param {Object} file
     * @returns {Object}
     */
    addAttachmentFile: function (file) {
        return this.callParent([file, true]);
    },
    initComponent: function () {

        var ticketStatus = TroubleTicket.config.ticketStates[this.initialConfig.record.get('status')];
        //  if status readOnly = false, you can edit so add a button
        if (ticketStatus && ticketStatus.readOnly === false) {
            // if there is only one button (the cancel button is added first)
            if (this.buttons.length === 1) {
                this.buttons.push({
                    text: TroubleTicket.util.i18n.translate('buttons.edit'),
                    cls: 'tt-edit-btn tt-primary-btn',
                    ctrAction: 'edit'
                });
            }
        } else {
            // remove the edit button from the array
            if (this.buttons.length === 2) {
                this.buttons.splice(1,1);
            }
        }
        this.callParent();
    }
});