/**
 * @class TroubleTicket.view.ticket.TicketForm
 * This view class is for the ticket open and edit action.
 */
Ext.define("TroubleTicket.view.ticket.TicketUpdateForm", {
    extend: 'TroubleTicket.view.ticket.TicketForm',
    alias: 'widget.ticketUpdateForm',
    requires: [
        'Ext.ux.form.field.StatusComboBox'
    ],
    items: [{
        xtype: 'fieldset',
        layout: 'column',
        cls: 'tt-custom-fieldset'
    }, {
        xtype: 'fieldset',
        layout: 'anchor',
        defaults: {
            anchor: '100%'
        },
        cls: 'tt-fixed-fieldset',
        items: [{
            xtype: 'fieldcontainer',
            layout: 'hbox',
            fieldLabel: TroubleTicket.util.i18n.translate('formLabels.issueTimePeriod'),
            labelAlign: 'right',
            items: [{
                xtype: 'displayfield',
                value:  TroubleTicket.util.i18n.translate('formLabels.from'),
                cls: 'tt-issuetime-from-prefix',
                margins: '0 10 0 10',
                flex: 0
            }, {
                xtype: 'displayfield',
                name: 'startTime',
                cls: 'tt-field-startTime',
                renderer: TroubleTicket.util.Date.formatDateTime,
                flex: 0
            }, {
                xtype: 'displayfield',
                value: TroubleTicket.util.i18n.translate('formLabels.to'),
                cls: 'tt-issuetime-to-prefix',
                margins: '0 10 0 10',
                flex: 0
            }, {
                xtype: 'displayfield',
                name: 'endTime',
                cls: 'tt-field-endTime',
                renderer: TroubleTicket.util.Date.formatDateTime,
                flex: 0
            }]
        }, {
            xtype: 'textfield',
            name: 'title',
            cls: 'tt-field-title',
            regex: /\w{1,}/,
            blankText: 'enter issue title',
            fieldLabel: TroubleTicket.util.i18n.translate('formLabels.issueTitle'),
            allowBlank: false,
            maxLength: 255,
            validateOnBlur: true
        }, {
            xtype: 'textfield',
            cls: 'tt-field-location',
            name: 'location',
            fieldLabel: TroubleTicket.util.i18n.translate('formLabels.issueLocation')
        }, {
            xtype: 'statusbox',
            name: 'status',
            fieldLabel: TroubleTicket.util.i18n.translate('formLabels.status')
        }, {
            xtype: 'displayfield',
            name: 'description',
            cls: 'tt-field-description',
            regex: /\w{1,}/,
            fieldLabel: TroubleTicket.util.i18n.translate('formLabels.description'),
            allowBlank: false,
            validateOnBlur: true
        }, {
            xtype: 'fieldcontainer',
            cls: 'tt-comments-fieldctr',
            fieldLabel: TroubleTicket.util.i18n.translate('formLabels.comment'),
            layout: {
                type: 'vbox',
                align: 'stretch'
            },
            width: '100%',
            items: [{
                xtype: 'textareafield',
                name: 'comment',
                cls: 'tt-field-comment',
                regex: /(|\w{1,})/,
                height: 60
            }]
        }, {
            xtype: 'hidden',
            cls: 'tt-field-id',
            name: 'id'
        }, {
            xtype: 'hidden',
            allowBlank: false,
            cls: 'tt-pdf-sourceapplication',
            name: 'sourceApplication'
        }, {
            xtype: 'fieldcontainer',
            cls: 'tt-attachments-fieldctr',
            fieldLabel: TroubleTicket.util.i18n.translate('formLabels.attachments'),
            layout: {
                type: 'vbox',
                align: 'stretch'
            },
            width: '100%',
            items: [{
                xtype: 'form',
                method: 'post',
                url: 'base64Encoder',
                name: 'file',
                cls: 'tt-add-attachment-form',
                items: [{
                    xtype: 'filefield',
                    buttonOnly: true,
                    buttonText: TroubleTicket.util.i18n.translate('formLabels.addAttachment')
                }]
            }]
        }]
    }]
});