/**
 * @class TroubleTicket.view.ticket.TicketForm
 * This view class is for the ticket open and edit action.
 */
Ext.define("TroubleTicket.view.ticket.TicketForm", {
    extend: 'Ext.form.Panel',
    alias: 'widget.ticketForm',
    requires: [
        'Ext.layout.container.Column',
        'Ext.form.FieldSet',
        'Ext.form.FieldContainer',
        'Ext.form.field.Date',
        'Ext.form.field.Time',
        'Ext.form.field.ComboBox',
        'Ext.form.field.Hidden',
        'Ext.form.field.File',
        'Ext.form.field.FileButton',
        'Ext.ux.form.field.Attachment',
        'Ext.ux.form.field.Comment',
        'TroubleTicket.config.view.ticketForm'
    ],
    bodyPadding: 10,
    overflowY: 'auto',
    cls: 'tt-ticket-form',
    fieldDefaults: {
        labelAlign: 'right',
        labelSeparator: '',
        labelWidth: 120
    },
    items: [{
        xtype: 'fieldset',
        layout: 'column',
        cls: 'tt-custom-fieldset'
    }, {
        xtype: 'fieldset',
        layout: 'anchor',
        defaults: {
            anchor: '100%'
        },
        cls: 'tt-fixed-fieldset',
        items: [{
            xtype: 'fieldcontainer',
            layout: 'hbox',
            fieldLabel: TroubleTicket.util.i18n.translate('formLabels.issueTimePeriod'),
            labelAlign: 'right',
            items: [{
                xtype: 'displayfield',
                value: TroubleTicket.util.i18n.translate('formLabels.from'),
                cls: 'tt-issuetime-from-prefix',
                margins: '0 10 0 10',
                flex: 0
            }, {
                xtype: 'displayfield',
                name: 'startTime',
                cls: 'tt-field-startTime',
                renderer: TroubleTicket.util.Date.formatDateTime,
                flex: 0
            }, {
                xtype: 'displayfield',
                value: TroubleTicket.util.i18n.translate('formLabels.to'),
                cls: 'tt-issuetime-to-prefix',
                margins: '0 10 0 10',
                flex: 0
            }, {
                xtype: 'displayfield',
                name: 'endTime',
                cls: 'tt-field-endTime',
                renderer: TroubleTicket.util.Date.formatDateTime,
                flex: 0
            }]
        }, {
            xtype: 'textfield',
            name: 'title',
            cls: 'tt-field-title',
            regex: /\w{1,}/,
            blankText: 'enter issue title',
            fieldLabel: TroubleTicket.util.i18n.translate('formLabels.issueTitle'),
            allowBlank: false,
            maxLength: 255,
            validateOnBlur: true
        }, {
            xtype: 'textfield',
            cls: 'tt-field-location',
            name: 'location',
            fieldLabel: TroubleTicket.util.i18n.translate('formLabels.issueLocation')
        }, {
            xtype: 'textareafield',
            name: 'description',
            cls: 'tt-field-description',
            regex: /\w{1,}/,
            fieldLabel: TroubleTicket.util.i18n.translate('formLabels.description'),
            allowBlank: false,
            validateOnBlur: true,
            height: 200
        }, {
            xtype: 'displayfield',
            fieldLabel: TroubleTicket.util.i18n.translate('formLabels.cqmPreview'),
            name: 'pdf',
            cls: 'tt-pdf-preview',
            hidden: true
        }, {
            xtype: 'hidden',
            cls: 'tt-field-id',
            name: 'id'
        }, {
            xtype: 'hidden',
            allowBlank: false,
            cls: 'tt-pdf-sourceapplication',
            name: 'sourceApplication'
        }, {
            xtype: 'fieldcontainer',
            cls: 'tt-attachments-fieldctr',
            fieldLabel: TroubleTicket.util.i18n.translate('formLabels.attachments'),
            layout: {
                type: 'vbox',
                align: 'stretch'
            },
            width: '100%',
            items: [{
                xtype: 'form',
                method: 'post',
                url: 'base64Encoder',
                name: 'file',
                cls: 'tt-add-attachment-form',
                items: [{
                    xtype: 'filefield',
                    buttonOnly: true,
                    buttonText: TroubleTicket.util.i18n.translate('formLabels.addAttachment')
                }]
            }]
        }]
    }],
    buttons: [{
        text: TroubleTicket.util.i18n.translate('buttons.cancel'),
        cls: 'tt-cancel-btn tt-secondary-btn',
        ctrAction: 'cancel'
    }, {
        text: TroubleTicket.util.i18n.translate('buttons.save'),
        cls: 'tt-submit-btn tt-primary-btn',
        ctrAction: 'save'
    }],
    /**
     * @cfg {Ext.data.Model} model
     * The model class to assign to the form.
     */
    model: 'TroubleTicket.model.ticket.Ticket',
    /**
     * @cfg {String} fixedFieldsFieldSetSelector
     * The component selector to find the fixed fields' fieldset.
     */
    fixedFieldsFieldSetSelector: 'fieldset[cls~=tt-fixed-fieldset]',
    /**
     * @cfg {String} customFieldsFieldSetSelector
     * The component selector to find the custom fields' fieldset.
     */
    customFieldsFieldSetSelector: 'fieldset[cls~=tt-custom-fieldset]',
    /**
     * @cfg {String} submitBtnSelector
     * The component selector to find the submit button. It is used in {@link #getSubmitBtn}.
     */
    submitBtnSelector: 'button[cls~=tt-submit-btn]',
    /**
     * @cfg {String} cancelBtnSelector
     * The component selector to find the cancel button. It is used in {@link #getCancelBtn}.
     */
    cancelBtnSelector: 'button[cls~=tt-cancel-btn]',
    /**
     * @cfg {String} attachmentFieldContainerSelector
     * The component selector to find the attachment field container.
     */
    attachmentFieldContainerSelector: 'fieldcontainer[cls~=tt-attachments-fieldctr]',
    /**
     * @cfg {String} fileUploadFormSelector
     * The component selector to find the form used to upload the attachment files.
     */
    fileUploadFormSelector: 'form[cls~=tt-add-attachment-form]',
    /**
     * @cfg {String} fileFieldSelector
     * The component selector to find the file field.
     */
    fileFieldSelector: 'form[cls~=tt-add-attachment-form] filefield',
    /**
     * @cfg {String} pdfPreviewFieldSelector
     * The component selector to find the pdf preview container.
     */
    pdfPreviewFieldSelector: 'displayfield[cls~=tt-pdf-preview]',
    /**
     * @cfg {String} commentsFieldCtrSelector
     * ComponentQuery selector for the comments display container
     */
    commentsFieldCtrSelector: 'fieldcontainer[cls~=tt-comments-fieldctr]',
    /**
     * @cfg {Object}
     * A ticket description object served by the API
     */
    ticket: null,
    /**
     * @private
     * Store the original values to check if the form values has been changed or not.
     */
    originalValues: null,

    /**
     * @cfg {Number|String}
     * The width value of the pdf object. Falsy value means that it is controlled via css
     */
    pdfPreviewWidth: null,

    /**
     * @cfg {Number|String}
     * The width value of the pdf object. Falsy value means that it is controlled via css
     */
    pdfPreviewHeight: null,

    /**
     * Override the Ext.form.Panel component initialization to:
     * - set the field change event handler
     * - call the display field initializer method.
     * - set the button click event handlers
     */
    initComponent: function () {
        var res = this.callParent(),
            form = this.getForm();
        this.setFormUpdateRecordMethod();
        form.getFields().each(this.setFieldOnChangeHandler, this);
        if (this.values) {
            this.setValues(this.values);
            delete this.values;
        }
        if (this.record) {
            this.setSubmitButtonState(this.record);
            this.loadRecord(this.record);
            delete this.record;
        }
        this.initButtons();
        return res;
    },
    /**
     * Initialize the buttons which are handled by the form itself. The action buttons' events are handled by the
     * controller.
     * @private
     */
    initButtons: function () {
        "use strict";
        var btn;
        btn = this.getAttachmentBtn();
        if (btn) {
            btn.on('change', this.onFileFieldChanged, this);
            btn.on('click', this.onFileFieldClick, this);
        }
    },
    /**
     * Override the original updateRecord method to add the associated data to the form.
     * @private
     */
    setFormUpdateRecordMethod: function () {
        var form = this.getForm(),
            me = this;
        Ext.override(form, {
            updateRecord: function () {
                var record = this.getRecord(),
                    values = this.getFieldValues(),
                    res = this.callOverridden(arguments);
                record.associations.each(function (assoc) {
                    var name = assoc.name,
                        store = record[name] ? record[name]() : null;
                    store.removeAll(true);
                    if (store && values[name]) {
                        store.add(values[name]);
                    }
                }, this);
                me.addNewCommentIfAny();
                me.setAttachmentButtonState();
                return res;
            }
        });
    },
    /**
     * Add the new comment from the 'comment' field if it has value.
     */
    addNewCommentIfAny: function () {
        var commentField = this.getForm().findField('comment'),
            comment = commentField && commentField.getValue(),
            record = this.getForm().getRecord(),
            commentStore = record && record.comments(),
            lastComment = commentStore.last();
        if (lastComment && lastComment.phantom) {
            if (comment) {
                lastComment.set('description', comment);
            } else {
                commentStore.remove(lastComment);
            }
        } else if (comment) {
            commentStore.add(commentStore.model.create({
                description: comment,
                created: TroubleTicket.util.Date.formatDateTime(
                    new Date(),
                    TroubleTicket.config.model.ticket.apiDateFormat
                )
            }));
        }
    },
    /**
     * Set a change event handler on the given field if the field is in the fixed fieldset.
     * @param {Ext.form.Field} field
     * @return {Ext.form.Field}
     */
    setFieldOnChangeHandler: function (field) {
        "use strict";
        if (field.up(this.fixedFieldsFieldSetSelector)) {
            field.on('change', this.handleFieldChange, this);
        }
        return field;
    },
    /**
     * Handle the event which has been triggered on a field when the value had changed,
     * don't check if the fields have changed until the form is rendered
     * @return {TroubleTicket.view.ticket.TicketForm} this
     */
    handleFieldChange: function (field, newValue, oldValue) {
        "use strict";
        if(this.rendered) {
            this.setSubmitButtonState();
        }
        return this;
    },
    /**
     * Disables the submits of the form.
     */
    disableFormSubmit: function () {
        "use strict";
        var btn = this.getSubmitBtn();
        if (btn) {
            btn.setDisabled(true);
        }
    },
    /**
     * Enables the submits of the form.
     */
    enableFormSubmit: function () {
        "use strict";
        var btn = this.getSubmitBtn();
        if (btn) {
            btn.setDisabled(false);
        }
    },
    /**
     * Set the enabled/disabled state of the file attach button, depending on the attachment limit.
     * @returns {TroubleTicket.view.ticket.TicketForm} this
     */
    setAttachmentButtonState: function () {
        "use strict";
        var attachmentBtn = this.getAttachmentBtn();
        if (attachmentBtn) {
            attachmentBtn.setDisabled(this.isAttachmentLimitReached());
        }
        return this;
    },
    /**
     * It check if the number of the attachments has already reached the limit or not.
     * @returns {boolean}
     */
    isAttachmentLimitReached: function () {
        "use strict";
        var record = this.getForm().getRecord(),
            cfg = record ? record.getCfg() : {},
            totalAttachments = this.getAttachmentCtr().items.filterBy(function (item) {
                return item.xtype === 'attachmentfield';
            }).length;
        return cfg.attachmentLimit && cfg.attachmentLimit <= totalAttachments;
    },
    /**
     * Event handler for the file field's click event. It checks the attachment limit.
     * @private
     */
    onFileFieldClick: function () {
        "use strict";
        var record = this.getForm().getRecord(),
            cfg = record ? record.getCfg() : {};
        if (this.isAttachmentLimitReached()) {
            var msg = {
                title: TroubleTicket.util.i18n.translate('attachment.uploadLimitReachedTitle'),
                msg: TroubleTicket.util.i18n.translate('attachment.uploadLimitReachedMsg', {
                    limit: cfg.attachmentLimit
                })
            };
            this.getApplication().fireEvent('showMessage', msg);
        }
    },
    /**
     * Event handler for the file field value change event.
     * @private
     */
    onFileFieldChanged: function (cmp, value, eOpts) {
        "use strict";
        var cfg;
        if (value && !this.isAttachmentLimitReached()) {
            this.down(this.fileUploadFormSelector).submit({
                success: this.onUploadSuccess,
                failure: this.onUploadFailed,
                scope: this
            });
            this.getAttachmentBtn().setLoading(true);
            this.disableFormSubmit();
        } else if (!value) {
            // if the value is not a proper path it is not necessary to do anything.
            // that's happening in IE8  because the change event is fire twice, so the second time the value is ""
            // http://www.sencha.com/forum/showthread.php?278383-Problem-with-IE-version-8-amp-version-9-after-using-xtype-filefield
        } else {
            cfg = this.getForm().getRecord().getCfg() || {};
            var msgConfig = { title: TroubleTicket.util.i18n.translate('attachment.uploadLimitReachedTitle'),
                msg: TroubleTicket.util.i18n.translate('attachment.uploadLimitReachedMsg', {
                    limit: cfg.attachmentLimit
                })
            };
            this.getApplication().fireEvent('showMessage', msgConfig);
        }
    },
    /**
     * Event handler for the upload form success event
     * @private
     */
    onUploadSuccess: function (form, action) {
        "use strict";
        var data = Ext.JSON.decode(action.response.responseText || '[]');
        this.getAttachmentBtn().setLoading(false);
        Ext.each(data.files, this.addAttachmentFile, this);
        this.enableFormSubmit();
        this.getAttachmentBtn().setLoading(false);
        this.setAttachmentButtonState();
    },
    /**
     * Event handler for the upload form success event
     * @private
     */
    onUploadFailed: function (form, action) {
        "use strict";
        var responseText = action.response.responseText,
            data = responseText ? Ext.JSON.decode(responseText) : null,
            msg = TroubleTicket.util.i18n.translate('attachment.uploadFailedMsg');
        this.getAttachmentBtn().setLoading(false);
        if (data) {
            msg = TroubleTicket.util.i18n.translate('attachment.error') || data.msg || msg;
        }
        this.enableFormSubmit();
        this.getAttachmentBtn().setLoading(false);

        var msg = {title: TroubleTicket.util.i18n.translate('attachment.uploadFailedMsgTitle'),
            msg : msg
        };
        this.getApplication().fireEvent('showMessage', msg);
    },
    /**
     * Add the attachment fields into the form by the given file array.
     * @param {Array} attachments The list of attachment files
     * @private
     */
    setAttachments: function (attachments) {
        Ext.each(attachments, function (file) {
            this.addAttachmentFile(file);
        }, this);
    },
    /**
     * Add the comment fields into the form by the given file array.
     * @param {Array} comments The list of comments
     * @private
     */
    setComments: function (comments) {
        var ctr = this.getCommentCtr();
        if (ctr) {
            if (comments && comments.length && !ctr.isVisible()) {
                ctr.show();
                ctr.setVisible(true);
            }
            Ext.each(comments, function (comment) {
                this.addComment(comment);
            }, this);
        }
    },
    /**
     * Returns with the number of the attachments.
     * @return {Number}
     */
    getTotalAttachments: function () {
        "use strict";
        var form = this.getForm(),
            values = form ? form.getFieldValues() : null,
            attachments = values ? values.attachments : null;
        return attachments ? (Ext.isObject(attachments) ? 1 : attachments.length) : 0;
    },
    /**
     * Add an attachment to the form.
     * @param {Object|Ext.data.Model} file Object example:
     * {
     *  "filename" : "test.pdf",
     *  "mimetype" : "application/pdf",
     *  "base64Data" : "WWEIUWIUDWIUDWIUDWUHBCWIUEHBCWIUHVBCIWEUVBCIWUVCIWUVCIWUVCIWUGVCIWVBC"
     * }
     * If the parameter is a model than the getData method will be called.
     * @param {Boolean} readOnly Indicates if the attachment is read only so it can be removed or not.
     */
    addAttachmentFile: function (file, readOnly) {
        "use strict";
        var ctr = this.getAttachmentCtr(),
            index = 0,
            field,
            file = file.getData ? file.getData() : file;

        if (file.id) {
            //if the attachment is already kept in the server side the delete link is not displayed
            file.showDeleteLink = false;
        } else {
            file.showDeleteLink = true;
        }
        //Force to display the delete button from the server side,
        //the server side allows to remove attachments
        if(TroubleTicket.vars.config.ticketDeleteAttachment == true || TroubleTicket.vars.config.ticketDeleteAttachment == 'true') {
            file.showDeleteLink = true;
        }
        field = Ext.ComponentManager.create({
            xtype: 'attachmentfield',
            value: file,
            name: 'attachments',
            readOnly: !!readOnly
        }, 'attachmentfield');
        if (field.isValid()) {
            index = ctr.items && ctr.items.length ? ctr.items.findIndex('id', ctr.items.last().id) : 0;
            ctr.insert(index, field).on('destroy', this.setAttachmentButtonState, this);
        } else {
            var msg = { title: TroubleTicket.util.i18n.translate('attachment.invalidFileTitle'),
                msg: TroubleTicket.util.i18n.translate('attachment.invalidFileMsg', file)
            };
            TroubleTicket.getApplication().fireEvent('showMessage', msg);
        }
    },
    /**
     * Add a comment to the form.
     * @param {Object|Ext.data.Model} comment Object example:
     * {
     *  "created" : "2013-12-24",
     *  "createdBy" : "654665585",
     *  "description" : "Some text and text"
     * }
     * If the parameter is a model than the getData method will be called.
     */
    addComment: function (comment) {
        "use strict";
        var ctr = this.getCommentCtr(),
            index,
            field;
        if (ctr) {
            if (comment.isModel) {
                comment = comment.getData(true);
            }
            field = Ext.ComponentManager.create({
                xtype: 'commentfield',
                value: comment,
                name: 'comments'
            }, 'attachmentfield');
            index = ctr.items && ctr.items.length ? 1 : 0;
            ctr.add(field);
        }
    },
    /**
     * Create a record of the model which has been set in the this.model property.
     * @param {Object} values The field and value pair for the model.
     * @returns {Ext.data.Model}
     */
    createRecordFromValues: function (values) {
        "use strict";
        if (this.model) {
            this.getForm()._record = Ext.ModelManager.getModel(this.model).createFromTicketRecord(values);
        }
        return this.getForm().getRecord();
    },
    /**
     * Return with the pdf Preview
     * @returns {Ext.Component|null}
     */
    getPdfPreview: function () {
        "use strict";
        return this.down(this.pdfPreviewFieldSelector);
    },
    /**
     * Return with the pdf object
     * @returns {Ext.button.Button|null}
     */
    getPdfPreviewObject: function () {
        "use strict";
        var ct = this.down(this.pdfPreviewFieldSelector),
            el;
        if (ct) {
            el = ct.el.down('object');
        }
        return el;
    },
    /**
     * Return with the field container of the attachment "fields" from the panel
     * found by the 'attachmentFieldContainerSelector' property.
     * @returns {Ext.form.FieldContainer|null}
     */
    getAttachmentCtr: function () {
        "use strict";
        return this.down(this.attachmentFieldContainerSelector);
    },
    /**
     * Return with the field container of the comment "fields" from the panel
     * found by the {@link commentsFieldCtrSelector} property.
     * @returns {Ext.form.FieldContainer|null}
     */
    getCommentCtr: function () {
        "use strict";
        return this.down(this.commentsFieldCtrSelector);
    },
    /**
     * Return with the attachment button.
     * @returns {Ext.form.field.FileButton}
     */
    getAttachmentBtn: function () {
        "use strict";
        return this.down(this.fileFieldSelector);
    },
    /**
     * Return with the cancel button from the panel found by the 'submitBtnSelector' property.
     * @returns {Ext.button.Button|null}
     */
    getSubmitBtn: function () {
        "use strict";
        var btn = this.down(this.submitBtnSelector);
        return btn || null;
    },
    /**
     * Return with the cancel button from the panel found by the 'cancelBtnSelector' property.
     * @returns {Ext.button.Button|null}
     */
    getCancelBtn: function () {
        "use strict";
        var btn = this.down(this.cancelBtnSelector);
        return btn || null;
    },
    /**
     * Handle the click event on the cancel button. It will close (destroy) the panel.
     * @private
     */
    onCancelBtnClick: function () {
        "use strict";
        this.close();
    },
    /**
     * Override the submit method to be sure that the record is updated
     * @returns {*|Object}
     */
    submit: function () {
        this.getForm().updateRecord();
        return this.callParent(arguments);
    },
    /**
     * Return with the fixed fields container (Ext.form.FieldSet).
     * @returns {Ext.form.FieldSet|null}
     */
    getFixedFieldsFieldSet: function () {
        "use strict";
        var fieldset = this.query(this.fixedFieldsFieldSetSelector);
        return fieldset && fieldset.length ? fieldset[0] : null;
    },
    /**
     * Return with the custom fields container (Ext.form.FieldSet).
     * @returns {Ext.form.FieldSet|null}
     */
    getCustomFieldsFieldSet: function () {
        "use strict";
        var fieldset = this.query(this.customFieldsFieldSetSelector);
        return fieldset && fieldset.length ? fieldset[0] : null;
    },
getAttachmentFieldCtr: function () {
        "use strict";
        var fieldset = this.query(this.attachmentFieldContainerSelector);
        return fieldset && fieldset.length ? fieldset[0] : null;
    },
    /**
     * It will set the record on the form and call the setValues method
     * @param record {Ext.data.Model}
     * @returns {*}
     */
    loadRecord: function (record) {
        "use strict";
        var form = this.getForm();
        form._record = record;
        form.url = record.getProxy().url;
        return this.setValues(record.getData(true), false);
    },
    /**
     * Returns true if any of the form fields has been changed
     * @returns {boolean}
     */
    isChanged: function () {
        "use strict";
//        return !Ext.Object.equals(this.originalValues, this.getForm().getFieldValues());
        return this.getForm().isDirty();
    },
    /**
     * Enable or disable the submit button depending on the action (add or edit) and the modifications on the fields.
     * @returns {TicketForm}
     */
    setSubmitButtonState: function (record) {
        "use strict";
        var record = record ? record : this.form.getRecord();
        var submitBtn = this.getSubmitBtn();
        if (submitBtn) {
            submitBtn.setDisabled(
                record.getData().id && !this.isChanged()
            );
        }
        return this;
    },
    /**
     * Override the original method to display the custom fields, check the submit button state and set the title.
     * @param {Object} values The new values for the fields
     * @param {Boolean} checkRecord Compare the record data with the given values and create one if not equal
     * @returns {Ext.form.Basic} this
     */
    setValues: function (values, checkRecord) {
        "use strict";
        var res,
            record = this.getForm().getRecord();
        checkRecord = typeof checkRecord === 'undefined' || !!checkRecord;
        if (!record || (record.getData(true) !== values && checkRecord)) {
            this.createRecordFromValues(values);
        }
        this.originalValues = values;
        this.addCustomFieldsByData(values);
        // every operation which call the setValues should be called after this
        res = this.form.setValues.apply(this.form, arguments);
        // If phantom = true, this record did not come from the server (update), it is a create/add
        if (this.getForm().getRecord().phantom === true) {
            this.addPdfPreview();
        }
        this.addAssociatedFields();

        this.setAttachmentButtonState();
        return res;
    },
    /**
     * Add an Ext.form.field.Field to the custom fieldset if the field doesn't exist yet.
     * @param {String} key The name/id of the field
     * @param {*} value The value of the field
     * @param {Ext.container.Container} ct Optional attribute for the container component. By default it's the custom
     * fieldset.
     * @returns {Ext.form.field.Field}
     */
    addCustomFieldByKeyValue: function (key, value, ct) {
        "use strict";
        var ctr = ct || this.getCustomFieldsFieldSet(),
            customFieldDescription = null,
            cfg = Ext.ns('TroubleTicket.config.app');

        var fieldCfg = {
                xtype: 'displayfield',
                name: key,
                cls: 'tt-field-' + key,
                fieldBodyCls: 'tt-wrapfield',
                value: value,
                fieldLabel: TroubleTicket.util.i18n.translate('customFieldDescriptions.'+key) === TroubleTicket.config.app.labelDefault  ? key : TroubleTicket.util.i18n.translate('customFieldDescriptions.'+key),
                renderer : function(value) {
                    return TroubleTicket.util.Date.applyDefaultDateFormat(value);
                }
            },
            res;
        if (key === 'imsi' && TroubleTicket.config.ticketImsi === 'restricted') {
            fieldCfg.hidden = true;
        }
        res = this.getForm().findField(fieldCfg.name);
        if (!res) {
            res = ctr.add(fieldCfg);
        }
        return res;
    },
    /**
     * Add custom fields to the custom fieldset from a ticket description's attributes. It will reset the custom
     * fieldset (remove all child component) and add the non-existing fields.
     * @param {Object} data Key value pair object. The key is the field name and the value is the value of the field.
     * @returns {TroubleTicket.view.ticket.TicketForm}
     */
    addCustomFieldsByData: function (data) {
        "use strict";
        var customFieldSet = this.getCustomFieldsFieldSet(),
            cfg = Ext.ns('TroubleTicket.config.view.ticketForm'),
            columnCfg = {
                xtype: 'panel',
                ui: 'transparent-panel'
            },
            record = this.getForm().getRecord(),
            associations = record.associations.keys,
            totalColumns,
            i;

        Ext.suspendLayouts();
        customFieldSet.removeAll();
        totalColumns = cfg.columns || 2;
        columnCfg.columnWidth = 1 / totalColumns;
        for (i = 0; i < totalColumns; i += 1) {
            customFieldSet.add(columnCfg);
        }
        Ext.Object.each(data, function (key, value) {
            var field;
            if (i >= totalColumns) {
                i = 0;
            }
            if (associations && associations.indexOf(key) === -1) {
                if (value) {
                    field = this.addCustomFieldByKeyValue(key, value, customFieldSet.items.getAt(i));
                    if (field.xtype !== 'hiddenfield') {
                        i += 1;
                    }
                }
            }
        }, this);
        Ext.resumeLayouts(true);
        return this;
    },
    /**
     * Used to add the associated fields to the form. It is looking for a special method named like:
     * 'setAssociationname'. If the method exists it will be called with the associated data.
     * @private
     */
    addAssociatedFields: function () {
        "use strict";
        var record = this.getForm().getRecord();
        if (record && record.associations) {
            record.associations.each(function (assoc) {
                var name = assoc.name,
                    data = record[name]().data.items,
                    fnName = 'set' + name.charAt(0).toUpperCase() + name.slice(1);
                if (data && Ext.isFunction(this[fnName])) {
                    this[fnName](data);
                }
            }, this);
        }
    },
    /**
     *
     * Insert PDF Preview field into form
     */
    addPdfPreview : function () {
        // Insert Trouble Ticket PDF preview
        "use strict";
        var pdfDisplayField = this.getPdfPreview(),
            attachments = this.getForm().getRecord().attachments();

        if ((!this.isIE()) && attachments && attachments.data.items.length) {
            pdfDisplayField.setValue('<object id="myPdf" type="application/pdf"' +
                (this.pdfPreviewWidth ? ' width="' + this.pdfPreviewWidth + '"' : '') +
                (this.pdfPreviewHeight ? ' width="' + this.pdfPreviewHeight + '"' : '') +
                'data="data:application/pdf;base64,' + attachments.first().getData().base64Data +
                '"></object>');
            pdfDisplayField.hidden = false;
        }
    },
    isIE : function () {
        "use strict";
        var myNav = navigator.userAgent.toLowerCase();
        return (myNav.indexOf('msie') !== -1) ? parseInt(myNav.split('msie')[1], 0) : false;
    }

});