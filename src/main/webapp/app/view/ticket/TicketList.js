/**
 * @class TroubleTicket.view.ticket.TicketList
 * This view class is for the ticket search action.
 */
Ext.define("TroubleTicket.view.ticket.TicketList", {
    extend: 'Ext.grid.Panel',
    alias: 'widget.ticketList',
    store: 'Tickets',
    shrinkWrap:true,
    border: false,

    /* Private properties/methods
     ======================================================================== */

    statics: {

        CUSTOM_RANGE: 'customRange',
        LAST_30_DAYS: 'last30Days',
        LAST_7_DAYS: 'last7Days'

    },

    /* Query search parameters
     ======================================================================== */
    /**
     * @cfg {String} searchFormSelector
     * The component selector to find the Search Form
     */
    searchFormSelector: 'form[cls~=tt-ticket-searchform]',
    /**
     * @cfg {String} searchValueFieldSelector
     * The component selector to find the Search Value Field
     */
    searchValueFieldSelector: 'textfield[cls~=tt-ticket-search-value]',
    /**
     * @cfg {String} searchTypeComboSelector
     * The component selector to find the Search Type Combo
     */
    searchTypeComboSelector: 'combo[cls~=tt-ticket-search-type]',
    /**
     * @cfg {String} searchTimePeriodSelector
     * The component selector to find the Search Time Period
     */
    searchTimePeriodSelector: 'combo[cls~=tt-time-period]',
    /**
     * @cfg {String} customDateRangeSelector
     * The component selector to find the Custom Date Range
     */
    customDateRangeSelector: '[cls~=tt-custom-date-range]',

    requires: [
        'TroubleTicket.store.Tickets',
        'TroubleTicket.config.view.ticketList'
    ],

    listeners: {
        itemclick: function (dv, record, item, index, e, eOpts) {
            Ext.Router.redirect('ticket/view/' + record.get('id') + '?sourceApplication=' + record.get('sourceApplication'));
        },
        afterLayout: function(it, layout, eOpts){
            TroubleTicket.app.viewport.setLoading(false);
        }
    },
    /**
     * @cfg {Ext.data.Model} columns
     * Column configuration for the grid.
     */
    columns: [
    ],

    title: TroubleTicket.util.i18n.translate('appTitle.edit'),

    padding: '0 0 30 0',
    /**
     * @cfg {Ext.data.Model} cls
     * CSS class applied to the grid
     */
    cls: 'tt-ticket-search-grid',

    header : {
        margin : '0 0 -2px 0'
    },

    viewConfig: {
        getRowClass: function (record, index) {
            // disabled-row - custom css class for disabled (you must declare it)
            var ticketStatus = TroubleTicket.config.ticketStates[record.get('status')];
            if ( ticketStatus && ticketStatus.readOnly) {
                return 'tt-row-disable';
            }
        }
    },

    items: [],

    dockedItems: [
        {
            xtype: 'toolbar',
            dock: 'top',
            margin: '0 0 10 10',
            items: [
                {
                    xtype: 'form',
                    cls: 'tt-ticket-searchform',
                    flex: 1,
                    layout: {
                        type: 'hbox',
                        align: 'bottom',
                        pack: 'start'
                    },
                    items: [

                        {
                            xtype: 'combo',
                            hideLabel: true,
                            cls : 'tt-ticket-search-type tt-split-dropdown-input',
                            displayField: 'name',
                            valueField: 'value',
                            typeAhead: true,
                            queryMode: 'local',
                            triggerAction: 'all',
                            name: 'searchType',
                            selectOnFocus: true,
                            width: 175,
                            margin : '0 10 0 0'
                        },
                        {
                            xtype: 'textfield',
                            cls : 'tt-ticket-search-value',
                            name: 'searchValue',
                            labelAlign: 'top',
                            margin: '0 10 0 0'
                        },
                        {
                            xtype: 'combo',
                            fieldLabel: TroubleTicket.util.i18n.translate('ticketSearch.timePeriod'),
                            labelAlign: 'top',
                            cls : 'tt-time-period tt-split-dropdown-input',
                            margin : '0 10 0 0',
                            displayField: 'name',
                            valueField: 'value',
                            typeAhead: true,
                            queryMode: 'local',
                            triggerAction: 'all',
                            name: 'timePeriod',
                            selectOnFocus: true,
                            width: 150,
                            store: Ext.create('Ext.data.Store', {
                                fields: ['name', 'value'],
                                data: [
                                    {
                                        value: 'last30Days',
                                        name: TroubleTicket.util.i18n.translate('ticketSearch.last30Days')
                                    },
                                    {
                                        name: TroubleTicket.util.i18n.translate('ticketSearch.today'),
                                        value: 'today'
                                    },
                                    {
                                        name: TroubleTicket.util.i18n.translate('ticketSearch.lastWeek'),
                                        value: 'lastWeek'
                                    },
                                    {
                                        name: TroubleTicket.util.i18n.translate('ticketSearch.custom'),
                                        value: 'custom'
                                    }
                                ]
                            }),
                            listeners: {
                                'select': function () {
                                    "use strict";
                                    var me = this.up('grid');
                                    var customDateRangeSelector = me.getCustomDateRange();

                                    if (this.value === 'custom' && customDateRangeSelector.length === 0) {
                                        this.up('grid').addCustomDatePicker();
                                    } else {
                                        this.up('grid').removeCustomDatePicker();
                                    }
                                }
                            }
                        },
                        {
                            xtype: 'button',
                            cls : 'tt-ticket-search-button tt-primary-btn',
                            text: TroubleTicket.util.i18n.translate('ticketSearch.searchButton')
                        }

                    ]

                }
            ]
        }
    ],
    /**
     * Override the Ext.form.Panel component initialization to:
     * - set the form initial values if there are get vars
     * - add columns based on the config
     * - add the action columns ('Edit')
     */
    initComponent: function () {

        this.callParent();
        this.suspendLayouts();

        this.createSearchTypeStore();

        if (this.extraParams) {
            this.setFormValues();
        } else {
            this.setSearchTypeValue();
        }
        this.setTimePeriod();
        this.addColumnsFromConfig();
        this.addActionColumns();

        this.resumeLayouts();
        return this;

    },
    /**
     * Set the form values based on the passed url parameters from the calling application
     * @private
     */
    setFormValues : function () {
        "use strict";

        var field = this.getSearchValueField();
        var key = this.extraParams.imsi ? this.extraParams.imsi : this.extraParams.msisdn;
        field.setValue(key);

        this.getSearchTypeCombo().setValue(this.extraParams.imsi ? 'imsi' : 'msisdn');

    },
    setTimePeriod : function () {
        "use strict";
        var cfg = Ext.ns('TroubleTicket.config.view.ticketList');
        this.getTimePeriod().setValue(cfg.defaultTimePeriod);
    },
    createSearchTypeStore : function () {

        var data= [];
        data.push({
            name: TroubleTicket.util.i18n.translate('ticketSearch.msisdn'),
            value: 'msisdn'
        });
        if (TroubleTicket.config.ticketImsi === 'unrestricted') {
            data.push({

                name: TroubleTicket.util.i18n.translate('ticketSearch.imsi'),
                value: 'imsi'
            });
        }
        this.getSearchTypeCombo().store =
            Ext.create('Ext.data.Store', {
                fields: ['name', 'value'],
                data: data
            });

    },
    setSearchTypeValue : function () {
        "use strict";
        var cfg = Ext.ns('TroubleTicket.config.view.ticketList');
        this.getSearchTypeCombo().setValue(cfg.defaultSearchType);
    },
    /**
     * Add the columns specified in app config.
     * @private
     */
    addColumnsFromConfig: function () {
        "use strict";
        var me = this,
            columns = [],
            config = this.getConfig();

        Ext.each(config.findTicketViewColumns, function () {
            columns.push(me.createColumn(this));
        });

        me.headerCt.add(columns);
    },
    /**
     * Create a column based on passed params
     * @param {object}
     * @return {Ext.form.column.Column}
     */
    createColumn: function (params) {
        return Ext.create('Ext.grid.column.Column', {
            text: params,
            dataIndex: params,
            flex: 1
        });
    },
    /**
     * Create a column based on passed params
     * @return {object}
     */
    getConfig: function () {
        "use strict";
        return TroubleTicket.config;
    },
    /**
     * Add the action columns
     * @private
     */
    addActionColumns: function () {
        "use strict";
        var me = this,
            header,

            actionColumns = Ext.create('Ext.grid.column.Action', {
                menuDisabled: true,
                text: TroubleTicket.util.i18n.translate('ticketSearch.actions'),
                items: [
                    {
                        tooltip: TroubleTicket.util.i18n.translate('ticketSearch.edit'),
                        sortable: false,
                        width: '100px',
                        cls: 'tt-ticket-edit',
                        margin: '0 10px 0 0',
                        icon: 'packages/troubleticket/resources/images/page/edit.png',  // Use a URL in the icon config
                        handler: function (grid, rowIndex, colIndex) {
                            var rec = grid.getStore().getAt(rowIndex);
                            Ext.ux.Router.redirect('ticket/edit/'+rec.data.id + '?sourceApplication=' + rec.get('sourceApplication'));
                            e.stopEvent();
                            Ext.EventObject.stopPropagation();
                        },
                        getClass: function(v, meta, rec) {  // Or return a class from a function
                            if (TroubleTicket.config.ticketStates[rec.get('status')].readOnly) {
                                this.items[1].icon = "";
                                return 'status-read-only';
                            }
                        }
                    }
                ]
            });

        me.headerCt.add(actionColumns);
    },
    /**
     * Add the custom date picker
     * @private
     */
    addCustomDatePicker: function () {
        "use strict";
        var me = this,
            today = new Date(),
            defTo = new Date(),
            defFrom = new Date(),
            cfg = Ext.ns('TroubleTicket.config.view.ticketList'),
            oldestDate = new Date();

        oldestDate.setDate(today.getDate() - cfg.dateLimit);

        var customDateRange = Ext.create('Ext.panel.Panel', {
            border: 0,
            layout: {
                type: 'hbox',
                pack: 'start'
            },
            cls: 'tt-custom-date-range',
            defaults: {
                xtype: 'datefield',
                border: 0,
                labelWidth: 40,
                width: 145,
                maxValue: today,
                minValue: oldestDate,
                margin : '0 10 0 0',
                format: cfg.format,
                submitFormat: cfg.submitFormat,
                msgTarget: 'side',
                allowBlank: false
            },
            items: [
                {
                    fieldLabel: TroubleTicket.util.i18n.translate('ticketSearch.from'),
                    name: 'from',
                    cls : 'tt-from',
                    value: defFrom,
                    labelAlign: 'top'
                },
                {
                    fieldLabel: TroubleTicket.util.i18n.translate('ticketSearch.to'),
                    cls : 'tt-to',
                    name: 'to',
                    value: defTo,
                    labelAlign: 'top',
                    listeners : {
                        change : function(params) {
                            me._validateCustomDateRange(params);
                        }
                    }
                }
            ]
        });

        var searchForm = me.getSearchForm();
        searchForm.insert(3, customDateRange);
    },
    _validateCustomDateRange: function (customDateRange) {

        var to = this.down('[cls~=tt-to]'),
            from = this.down('[cls~=tt-from]');

        if(to.getValue() < from.getValue()) {
            // Invalid date: Start date must be less than end date.
            from.markInvalid('From date must be less than To Date');
            to.markInvalid('From date must be less than To Date');
        }
    },
    /**
     * Remove the custom date picker
     * @private
     */
    removeCustomDatePicker: function () {
        "use strict";
        var customDateRange = this.getCustomDateRange();
        if (customDateRange.length > 0) {
            customDateRange[0].destroy();
        }
    },

    /**
     * Return with the customDateRange fields
     * @returns {Ext.form.FieldContainer|null}
     */
    getCustomDateRange: function () {
        "use strict";
        return Ext.ComponentQuery.query(this.customDateRangeSelector);
    },

    /**
     * Return with the search form
     * @returns {Ext.form|null}
     */
    getSearchForm: function () {
        "use strict";
        return this.down(this.searchFormSelector);
    },
    /**
     * Return with the field for search value
     * @returns {Ext.form.Field|null}
     */
    getSearchValueField : function () {
        "use strict";
        return this.down(this.searchValueFieldSelector);

    },
    /**
     * Return with the search type field
     * @returns {Ext.form.field.ComboBox|null}
     */
    getSearchTypeCombo : function () {
        "use strict";
        return this.down(this.searchTypeComboSelector);

    },
    /**
     * Return with the field container of the time period drop down from the form panel
     * @returns {Ext.form.field.ComboBox|null}
     */
    getTimePeriod : function () {
        "use strict";
        return this.down(this.searchTimePeriodSelector);

    }
});