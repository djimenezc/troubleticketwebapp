/**
 * This panel will hold the base of the laout. It will load automatically the
 * {@link TroubleTicket.view.shared.PageHeader pageHeader} and the
 * {@link TroubleTicket.view.shared.PageBody pageBody}
 */
Ext.define('TroubleTicket.view.Main', {
    extend: 'Ext.container.Container',
    requires: [
        'Ext.layout.container.Border',
        'TroubleTicket.view.shared.PageHeader',
        'TroubleTicket.view.shared.PageBody'
    ],
    xtype: 'app-main',
    layout: 'border',
    ui: 'transparent-panel',
    cls: 'tt-app-ctr',
    defaults: {
        ui: 'transparent-panel',
        layout: {
            type: 'vbox',
            align: 'center'
        },
        defaults: {
            width: '100%',
            maxWidth: 960,
            ui: 'transparent-panel'
        }
    },
    items: [{
        xtype: 'pageHeader',
        region: 'north'
    }, {
        xtype: 'pageBody',
        region: 'center',
        overflowY: 'auto'
    }]
});