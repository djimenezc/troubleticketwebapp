/**
 * @class TroubleTicket.controller.TicketList
 * The controller class to handle the Ticket Search action.
 * @extends Ext.app.Controller
 * @singleton
 */
Ext.define('TroubleTicket.controller.TicketList', {
    extend: 'Ext.app.Controller',
    views: ['ticket.TicketList'],
    models: [
        'ticket.Ticket'
    ],
    stores: [
        'Tickets'
    ],
    requires: [
        'TroubleTicket.config.controller.ticket', // the configuration of the controller
        'TroubleTicket.store.Tickets' // the configuration of the controller
    ],
    /**
     * As in every controller can have routes property which is used by the Ext.ux.Router via the application.
     * The keys are the url fragment and the values are the controller and methods.
     * https://github.com/brunotavares/Ext.ux.Router/
     * @private
     */
    routes: {
        'ticket': 'ticketList#actionList',
        'ticket/list': 'ticketList#actionList',
        'ticket/list?*query': 'ticketList#actionList'
    },
    _searchButtonPressed : false,
    _searchQueryObject : null,
    _sourceApplication : null,
    /**
     * Component query selectors
     */
    searchFormSelector: 'form[cls~=tt-ticket-searchform]',
    searchGridSelector: 'grid[cls~=tt-ticket-search-grid]',
    /**
     * Initialize the controller: add the handlers for the form panel
     * @return {TroubleTicket.controller.Ticket}
     */
    init: function () {
        "use strict";

        this.control({
            'ticketList button': {
                click: this.onTicketSearch
            }
        });
        return this;
    },

    onStoreLoadRecord: function (records, operation, success) {

        "use strict";

        var view = operation && operation.view ? operation.view : 'ticket.TicketList',
            store = this.getStore('Tickets'),
            searchGrid,
            msg ={};

        if (success) {
            if (store.count() > 0) {
                if (this._searchButtonPressed) {
                    searchGrid = Ext.ComponentQuery.query('grid')[0];
                    searchGrid.getView().refresh();
                } else {
                    this.runView(view, operation.title, operation.path, operation.params);
                }
            } else {
                msg.title = TroubleTicket.util.i18n.translate('ticketSearch.error.notRecordsFound.title');
                msg.msg = TroubleTicket.util.i18n.translate('ticketSearch.error.notRecordsFound.msg');

                this.getApplication().fireEvent('showMessage', msg);
            }
        } else {
           this.getApplication().fireEvent('labelErrorHandler',records, operation, success, store);
        }
    },
    /**
     * Returns with an object with two property:
     * - msg: the message for the operation result
     * - title: the title for the operation result
     * @param {Ext.data.Operation} operation
     * @param {Object} [data] The data object to apply to the message
     * @param {Object} [defaults]
     * @returns {Object}
     */
    getOperationMsgData: function (operation, data, defaults) {
        "use strict";
        var res = defaults || {},
            error,
            action,
            success;
        if (operation && operation.action) {
            error = operation.error;
            action = operation.action;
            success = operation.success;
            res.msg = TroubleTicket.util.i18n.translate(action + 'Ticket.' + (success ? 'success' : 'failure') +
                (error && error.status ? '.' + error.status : '') + '.msg');

            res.title = TroubleTicket.util.i18n.translate(action + 'Ticket.'  + (success ? 'success' : 'failure') +
                (error && error.status ? '.' + error.status : '') +
                '.title');
        }
        return res;
    },
    /**
     * Try to get or load the record for the given ID and run the given view in the application.
     * @param {String/Object/Ext.view.View} view
     * @param {Object} queryObject contains imsi or msisdn query parameters
     * @param {String} title
     * @param {Array/Object} path
     * @returns {TroubleTicket.controller.Ticket} this
     */
    getRecordByIdAndRunView: function (view, queryObject, title, path) {
        "use strict";
        var store = this.getStore('Tickets');
        view = view || 'ticket.TicketView';
        this._searchQueryObject = queryObject;
        store.load({
            view: view,
            title: title,
            path: path,
            callback: this.onStoreLoadRecord,
            scope: this,
            params: queryObject
        });
        return this;
    },
    /**
     * Run the given view in the application
     * @param {Ext.Component} view A class or object to add the application container
     * @param {String} title The title of the current page (Optional)
     * @returns {Ext.Component}
     * @returns {boolean} Return false if the container element of the application doesn't exist
     */
    runView: function (view, title, path, extraParams) {
        "use strict";
        return this.getApplication().runView(this.getView(view).create({
            title: title,
            extraParams : extraParams
        }));
    },
    /**
     * It is the listing action which can be routed by the Ext.ux.Router. This will run the ticket list view via the
     * application {@link TroubleTicket.Application#runView runView} method
     * @return {Boolean} It will return with the response of the application which can be a FALSE
     * boolean value
     * @return {Ext.panel.Panel} or the view panel which has been added to the controller container.
     */
    actionList: function () {
        "use strict";
        this._searchButtonPressed = false;
        var dateRange;

        //clear the store before reload it, using silent mode
        this.getStore('Tickets').removeAll(true);
        var queryObject = TroubleTicket.util.Util.getQueryParameters();
        this._sourceApplication = TroubleTicket.util.Util.getQueryParameter('sourceApplication');

        if (!queryObject || !this._sourceApplication) {
            var msg = {};
            msg.title = TroubleTicket.util.i18n.translate('messageWindow.title.error') ;
            msg.msg = TroubleTicket.util.i18n.translate('validationError.sourceApplication');
            this.getApplication().fireEvent('showMessage', msg);
            return;
        }
        // If there is a queryObject imsi in the URL, we are coming from an external application
        if (queryObject.imsi || queryObject.msisdn) {
            // default date range is last 30 days
            dateRange = this.getDates('last30days');
            queryObject.dateFrom = dateRange.from;
            queryObject.dateTo = dateRange.to;
            // _searchQueryObject is set before we go to read record and go to edit/view
        } else if (this._searchQueryObject) {
            queryObject = this._searchQueryObject;
        }
        else {
            // If there is no query string, we are showing the default empty view.
            this.runView('ticket.TicketList',TroubleTicket.util.i18n.translate('appTitle.list'));
            return this;
        }

        this.getRecordByIdAndRunView(
            'ticket.TicketList',
            queryObject,
            TroubleTicket.util.i18n.translate('appTitle.list')
        );

        return this;
    },

    /**
     * It is the view ticket action which can be routed by the Ext.ux.Router. This will run the view ticket view
     * via the application {@link TroubleTicket.Application#runView runView} method.
     * @returns {TroubleTicket.controller.Ticket} this
     */
    actionSearch: function (params) {
        "use strict";
        this.getRecordByIdAndRunView(params.id, 'ticket.TicketView');

        return this;
    },
    onTicketSearch : function() {
        "use strict";
        var queryObject={},
            ticketSearchForm = this.getTicketSearchForm();
        this._searchButtonPressed = true;
        queryObject[ticketSearchForm.getValues().searchType] = ticketSearchForm.getValues().searchValue;
        queryObject.sourceApplication = this._sourceApplication;
        var dates = this.getDates(ticketSearchForm.getValues().timePeriod, ticketSearchForm.getValues().from, ticketSearchForm.getValues().to);

        queryObject.dateFrom = dates.from;
        queryObject.dateTo = dates.to;

        this.getRecordByIdAndRunView(
            'ticket.TicketList',
            queryObject,
            TroubleTicket.util.i18n.translate('appTitle.list')
        );

    },
    getTicketSearchForm : function () {
        return Ext.ComponentQuery.query(this.searchFormSelector)[0];
    },
    /***
     * Return the start and end date depending on a timeperiod and from and to dates
     * @param timePeriod (string)
     * @param from (date)
     * @param to (date)
     */
    getDates : function(timePeriod, from, to) {
        "use strict";

        var dates = {},
            today = new Date();
        dates.from = new Date();
        dates.to = new Date();

        switch(timePeriod)
        {
            case 'custom':
                dates.from = new Date(from);
                dates.to = new Date(to);
                break;
            case 'lastWeek':
                dates.to = today;
                dates.from.setDate(today.getDate() - 7);
                break;
            case 'last30Days':
                dates.to = today;
                dates.from.setDate(today.getDate() - 30);
                break;
            case 'today':
                break;
            default:
                dates.from.setDate(today.getDate() - 30);
                break;
        }
        dates.from = (Ext.Date.format(dates.from, "Y-m-d"));
        dates.to = (Ext.Date.format(dates.to, "Y-m-d"));
        return dates;
    }
});