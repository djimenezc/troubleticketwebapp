/**
 * @class TroubleTicket.controller.Ticket
 * The controller class to handle the Ticket related actions.
 * @extends Ext.app.Controller
 * @singleton
 */
Ext.define('TroubleTicket.controller.Ticket', {
    extend: 'Ext.app.Controller',
    views: ['ticket.TicketForm', 'ticket.TicketView', 'ticket.TicketUpdateForm'],
    models: [
        'ticket.Ticket'
    ],
    stores: [
        'Tickets'
    ],
    requires: [
        'TroubleTicket.config.controller.ticket', // the configuration of the controller
        'TroubleTicket.store.Tickets', // the configuration of the controller
        'Ext.util.DelayedTask'
    ],
    /**
     * As in every controller can have routes property which is used by the Ext.ux.Router via the application.
     * The keys are the url fragment and the values are the controller and methods.
     * https://github.com/brunotavares/Ext.ux.Router/
     * @private
     */
    routes: {
        'ticket/open': 'ticket#actionOpen',
        'ticket/add': 'ticket#actionOpen',
        'ticket/edit/:id?*query': 'ticket#actionEdit',
        'ticket/view/:id?*query': 'ticket#actionView',
        'ticket/edit/:id': 'ticket#actionEdit',
        'ticket/view/:id': 'ticket#actionView'
    },

    actionViews: {
        create: 'ticket.TicketForm',
        update: 'ticket.TicketUpdateForm',
        read: 'ticket.TicketView'
    },
    _searchQueryObject : null,
    _sourceApplication : null,
    /**
     * Initialize the controller: add the handlers for the form panel
     * @return {TroubleTicket.controller.Ticket}
     */
    init: function () {
        "use strict";
        this.control({
            '.ticketForm .button': {
                click: this.onTicketFormButtonAction
            }
        });
        this._searchQueryObject = TroubleTicket.util.Util.getQueryParameters();
        this._searchQueryObject.dataMode= 'SUMMARY';
        this._sourceApplication = TroubleTicket.util.Util.getQueryParameter('sourceApplication');

        this.application.on({
            deleteAttachment : this.deleteAttachment,
            scope : this
        });

        return this;
    },

    refs : [{
        ref: 'ticketForm',
        selector: 'ticketForm'
    }],

    /**
     * General event handler on the TicketForm panel buttons' click event. The important part of the buttons is the
     * 'ctrAction' property which will define the method called in the controller. So in this case we don't have to
     * add new and new methods to the form view but we can handle all of these actions (events) in the controller.
     * @private
     */
    onTicketFormButtonAction: function (btn) {
        "use strict";
        var action = btn.ctrAction;
        if (action) {
            action = 'ticketForm' + action.charAt(0).toUpperCase() + action.slice(1);
            if (Ext.isFunction(this[action])) {
                this[action](btn);
            }
        }
    },
    /**
     * Returns with the TicketForm panel if exists.
     * @return {TroubleTicket.view.ticket.TicketForm}
     * @return {undefined}
     */
    getTicketFormPanel: function () {
        "use strict";
        var vp = this.getApplication().getViewport(),
            res = vp ? vp.down('.ticketForm') : null;
        return res;
    },
    /**
     * Set the loading mask on the form panel.
     * @param loading {Boolean/Object} It can be boolean and in this case the true means that the form will be masked
     * the false is the opposite. If the parameter is an object it will be passed as a LoadMask configuration.
     * @return {TroubleTicket.view.ticket.TicketForm} Returns with the form panel if found.
     * @return {Null} Returns with null if not found.
     */
    setLoadingTicketFormPanel: function (loading) {
        "use strict";
        var cfg;
        cfg = loading || typeof loading === 'undefined' ? (Ext.isObject(loading) ? loading : {}) : false;
        if (cfg) {
            Ext.applyIf(cfg, {msg: TroubleTicket.util.i18n.translate('createTicket.loadingMsg')});
        }
        TroubleTicket.getApplication().getViewport().setLoading(cfg);

        return TroubleTicket.getApplication().getViewport();
    },
    /**
     * This is the button event handler for the TicketForm#submit button. It is called by the
     * {@link #onTicketFormButtonAction} method which is the general button handler for the
     * {@link TroubleTicket.view.ticket.TicketForm TicketForm} buttons
     * @private
     */
    ticketFormSave: function () {
        "use strict";
        var formPanel = this.getTicketFormPanel(),
            form = formPanel ? formPanel.getForm() : null,
            storedRecord,
            record,
            changes;
        if (form && form.isValid()) {
            form.updateRecord();
            record = form.getRecord() || this.getModel('ticket.Ticket').create(form.getValues());
            storedRecord = record.getId() ? this.getStore('Tickets').getById(record.getId()) : false;
            changes = record.getChanges() && (!storedRecord
                || (storedRecord.getAssociatedData() !== record.getAssociatedData()));
            if (record.validate().isValid() && (record.phantom || !Ext.Object.isEmpty(changes) || changes === true)) {
                this.saveRecord(record);
            }
        }
    },
    /**
     * This is the button event handler for the TicketForm#cancel button. It is called by the
     * {@link #onTicketFormButtonAction} method which is the general button handler for the
     * {@link TroubleTicket.view.ticket.TicketForm TicketForm} buttons
     * @private
     */
    ticketFormCancel: function () {
        "use strict";

        this.setLoadingTicketFormPanel({
            msg: TroubleTicket.util.i18n.translate('loadingMsg')
        });
        var record = this.getSelectedTicketRecord(),
            formPanel = this.getTicketFormPanel(),
            form = formPanel ? formPanel.getForm() : null;

        if (form && form.isDirty() && record) {
            record.reject();
        }
        Ext.Router.redirect('ticket/list?sourceApplication='+ this._sourceApplication);
    },
    /**
     * This is the button event handler for the TicketForm#submit button. It is called by the
     * {@link #onTicketFormButtonAction} method which is the general button handler for the
     * {@link TroubleTicket.view.ticket.TicketForm TicketForm} buttons
     * @private
     */
    ticketFormEdit: function () {
        "use strict";
        var id = this.getSelectedTicketId();

        if (id) {
            Ext.Router.redirect('ticket/edit/' + id + '?sourceApplication='+ this._sourceApplication);
        }
    },
    /**
     * Event handler for the ticket form 'beforeaction' event. It will prevent the original action by returning FALSE
     * and will save the record of the form if it has any or create one and save it if the form has no any.
     * We use the model to save the data because it has a built-in RESTful solution and also we can use it anywhere not
     * just in the add/update ticket action.
     * @private
     * @param {Ext.form.Basic} form
     * @returns {boolean} It always return false to prevent the original action.
     */
    onFormBeforeAction: function (form) {
        "use strict";
        form.updateRecord();
        var record = form.getRecord() || this.getModel('ticket.Ticket').create(form.getValues());
        if ((record.phantom || !Ext.Object.isEmpty(record.getChanges())) && record.validate().isValid()) {
            this.saveRecord(record);
        }
        return false;
    },
    /**
     * Set the handler methods and save the given record.
     * @private
     * @param {TroubleTicket.model.ticket.Ticket} record The record to save
     */
    saveRecord: function (record) {
        "use strict";
        this.setLoadingTicketFormPanel({
            msg: TroubleTicket.util.i18n.translate((record.phantom ? 'create' : 'update') + 'Ticket.loadingMsg')
        });
        record.save({
            success: this.onModelSaveSuccess,
            failure: this.onModelSaveFailure,
            scope: this
        });
    },
    /**
     * This event handler is setup in the {@link #saveRecord} method to handler the successful response from the server.
     * @param {Ext.data.Model} record
     * @param {Ext.data.Operation} operation
     */
    onModelSaveSuccess: function (record, operation) {
        "use strict";
        var dataNs = Ext.ns('TroubleTicket.config.controller.ticket.postTicketNs'),
            action = operation.action,
            store = this.getStore('Tickets'),
            prop,
            data,
            msg,
            msgWin;
        dataNs = dataNs.split('.');
        prop = dataNs.pop();
        if (dataNs.length) {
            data = Ext.ns(dataNs.join('.'));
        } else {
            data = window;
        }
        if (data[prop]) {
            delete data[prop];
        }
        if (store.find('id', record.get('id')) > -1) {
            store.add(record);
        }
        msg = TroubleTicket.util.i18n.getOperationMsgData(operation, record.getData(), {
            title: TroubleTicket.util.i18n.translate( action + 'Ticket.success.title'),
            msg: TroubleTicket.util.i18n.translate(action + 'Ticket.success.msg', record.getData())
        });
        var me = this;
        var callbackFn = function () {
            me.setLoadingTicketFormPanel({
                msg: TroubleTicket.util.i18n.translate('loadingMsg')
            });
            Ext.Router.redirect('ticket/view/' + record.get('id') + '?sourceApplication=' + me._sourceApplication);
        };
        //FIXME this.application.i18n.t('Ticket:' + action + 'Ticket.success.buttonText')
        this.getApplication().fireEvent('showMessage', msg, callbackFn());
        this.setLoadingTicketFormPanel(false);
    },
    /**
     * This event handler is setup in the {@link #saveRecord} method to handler the failure response from the server.
     * @param {Ext.data.Model} record
     * @param {Ext.data.Operation} operation
     */
    onModelSaveFailure: function (record, operation) {
        "use strict";
        var msg, msgWin;

        if (!TroubleTicket.util.Util.isClientServerRequestError(operation.serverResponse.status)){
            //TODO move to the MessageController and create a generic mechanism to show popups with two buttons
            msg = TroubleTicket.util.i18n.getOperationMsgData(operation, {
                msg: TroubleTicket.util.i18n.translate('createTicket.failure.msg'),
                title: TroubleTicket.util.i18n.translate('createTicket.failure.title')
            });

            msgWin = new Ext.window.MessageBox({
                buttonAlign: 'right',
                modal: true,
                bodyPadding: '0 0 10 0',
                padding: 10,
                buttons: [{
                    text: TroubleTicket.util.i18n.translate('createTicket.failure.cancelButtonText'),
                    cls: 'tt-secondary-btn',
                    handler: function () {
                        msgWin.close();
                    },
                    scope: this
                }, {
                    text: TroubleTicket.util.i18n.translate('createTicket.failure.tryButtonText'),
                    cls: 'tt-try-btn tt-primary-btn',
                    handler: function () {
                        msgWin.close();
                        this.saveRecord(record);
                    },
                    scope: this
                }]
            });
            
            msgWin.show({
                title: TroubleTicket.util.i18n.translate('createTicket.failure.title'),
                msg: msg.msg,
                width: 675,
                modal: true,
                closable: false
            });
        }
        this.setLoadingTicketFormPanel(false);
    },
    /**
     * Event handler to handle the ticket form close event. It will ust redirect to the list url via Ext.Router
     * @private
     */
    onFormClose: function () {
        "use strict";
        if (window !== window.parent) {
            window.close();
        } else {
            Ext.Router.redirect('ticket/list?sourceApplication='+ this._sourceApplication);
        }
    },
    /**
     * It will return an instance of the {@link TroubleTicket.model.ticket.Ticket} initialized with the posted
     * fields by the CQM.
     * @return {TroubleTicket.model.ticket.Ticket} The instantiated model for the arrived data
     * @return {False} Return false if the data is empty
     */
    createTicketModelFromPostVars: function () {
        "use strict";
        var data = Ext.ns(Ext.ns('TroubleTicket.config.controller.ticket.postTicketNs')),
            model = false;
        if (data && !Ext.Object.isEmpty(data)) {
            model = Ext.create('TroubleTicket.model.ticket.Ticket', data);
        }
        return model;
    },
    /**
     * The callback method for the store load result, called from getRecordByIdAndRunView
     * (indirectly from actionEdit and actionView).
     * @param {TroubleTicket.model.ticket.Ticket} records The loaded records
     * @param {Ext.data.Operation} operation
     * @param {Boolean} success
     * @private
     */
    onStoreLoadRecord: function (records, operation, success) {
        "use strict";
        var view = operation && operation.view ? operation.view : 'ticket.TicketView',
            record = this.getStore('Tickets').getById(operation && operation.id),
            errors = record ? record.validate() : true,
            msgData,
            msg;

        if (success && record && this._sourceApplication !== null) {
            if (typeof record.commentsStore !== 'undefined') {
                record.commentsStore.sort('created', 'DESC');
            }
            this.runViewWithRecord(view, record, operation.title, operation.path);
        } else if (!TroubleTicket.util.Util.isClientServerRequestError(operation.serverResponse.status)){
            msgData = record ? record.getData() : {};
            if (errors && errors.length) {
                msgData.errorList = record.getErrorListHtml();
            }
            msg = this.getOperationMsgData(operation, msgData, {
                msg: 'Error while loading item.<br/>__errorList__',
                title: 'Error'
            });
            var callbackFn = function () {
                Ext.ux.Router.redirect('ticket/list?sourceApplication='+ this._sourceApplication);
            };
            this.getApplication().fireEvent('showMessage', msg,  callbackFn);
        }

        /*TODO Hack,TP-34225
          * if the view is not visible, it is not possible to calculate the right height for the comments,
         * so applying a little delay, the layout view is updated in the very end of the render process, it is necessary
         * that the view is displayed to perform a rights height calculations.
         * Problem related with the way the view is create and add to the view port, and that the comments use tpl with
         * dinamic content.
          */
        var delayTask = new Ext.util.DelayedTask(function() {
            var ticketFormView = this.getTicketForm();
            if(ticketFormView) {
                ticketFormView.updateLayout();
            }
        },this);

        delayTask.delay(10);
    },
    /**
     * Try to get or load the record for the given ID and run the given view in the application.
     * @param {String/Object/Ext.view.View} view
     * @param {Number} id
     * @param {String} title
     * @param {Array/Object} path
     * @returns {TroubleTicket.controller.Ticket} this
     */
    getRecordByIdAndRunView: function (view, id, title, path) {
        "use strict";
        var store = this.getStore('Tickets'),
            extraParams = TroubleTicket.util.Util.getQueryParameters(),
            record = store.getById(id);

        extraParams.dataMode = 'SUMMARY';

        view = view || 'ticket.TicketView';
            store.load({
                id: id,
                addRecords: true,
                view: view,
                title: title,
                path: path,
                params: extraParams,
                callback: this.onStoreLoadRecord,
                scope: this
            });
        return this;
    },
    /**
     * Run the given view in the application and add the record as a confing
     * @param {String} action A class or object to add the application container
     * @param {Ext.data.Model} record A model instance to pass to the view
     * @param {String} title The title of the current page (Optional)
     * @param {Array|Object} path An application breadcrumb path,
     * see {@link TroubleTicket.view.shared.AppBreadcrumb#setPath} (Optional)
     * @returns {Ext.Component}
     * @returns {boolean} Return false if the container element of the application doesn't exist
     */
    runViewWithRecord: function (action, record, title, path) {
        "use strict";
        var view = this.actionViews[action] || action,
            cfg = TroubleTicket.config.ticketStates,
            status = record && record.get('status'),
            res;

        if (action === this.actionViews.update && cfg[status].readOnly) {
            view = this.actionViews.read;
            var msgConfig = { title: TroubleTicket.util.i18n.translate('readOnlyState.title'),
                msg: TroubleTicket.util.i18n.translate('readOnlyState.msg', {status: status})};

            this.getApplication().fireEvent('showMessage', msgConfig);
        }
        res = this.getApplication().runView(this.getView(view).create({
            record: record,
            title: title,
            path: path
        }));
        return res;
    },
    /**
     * It is the listing action which can be routed by the Ext.ux.Router. This will run the ticket list view via the
     * application {@link TroubleTicket.Application#runView runView} method
     * @return {Boolean} It will return with the response of the application which can be a FALSE
     * boolean value
     * @return {Ext.panel.Panel} or the view panel which has been added to the controller container.
     */
    actionList: function () {
        "use strict";
        var app = this.application;
        return app.runView(this.getView('ticket.TicketList').create({
            title: TroubleTicket.util.i18n.translate('appTitle.list')
        }));
    },
    /**
     * It is just an alias for the actionOpen.
     * @returns {Boolean/Ext.panel.Panel}
     */
    actionAdd: function () {
        "use strict";
        return this.actionOpen.apply(this, arguments);
    },
    /**
     * It is the add ticket action which can be routed by the Ext.ux.Router. This will run the ticket add/edit form view
     * via the application {@link TroubleTicket.Application#runView runView} method.
     * @returns {Boolean/Ext.panel.Panel} It will return with the response of the application which can be a FALSE
     * boolean value or the view panel which has been added to the controller container.
     */
    actionOpen: function () {
        "use strict";
        var model = this.createTicketModelFromPostVars(),
            errors = model && model.validate(false),
            res = false,
            msg = '';
        this._sourceApplication = model.get('sourceApplication');
        if (model && errors.isValid() && this._sourceApplication) {
            this.runViewWithRecord('ticket.TicketForm', model, TroubleTicket.util.i18n.translate('appTitle.open'));
        } else if (!this._sourceApplication) {
            this.getApplication().fireEvent('showMessage', {
                title : 'Error',
                msg : TroubleTicket.util.i18n.translate('validationError.sourceApplication')
            });
        } else {
            msg = model ? model.getErrorListHtml(false) : '';
            msg = TroubleTicket.util.i18n.translate('invalidTicketPost.msg', {errors: msg});

            var msg = {
                title: TroubleTicket.util.i18n.translate('invalidTicketPost.title'),
                msg : msg
            };
            var me = this;
            this.getApplication().fireEvent('showMessage', msg, function () {
                Ext.ux.Router.redirect('ticket/list?sourceApplication=' + me._sourceApplication);
            });
        }
        return res;
    },
    /**
     * It is the edit ticket action which can be routed by the Ext.ux.Router. This will run the ticket add/edit form
     * view via the application {@link TroubleTicket.Application#runView runView} method.
     * @param {Object} params The given parameters, eg.: {id: 3}
     * @returns {TroubleTicket.controller.Ticket}
     */
    actionEdit: function (params) {
        "use strict";
        this.setLoadingTicketFormPanel({
            msg: TroubleTicket.util.i18n.translate('loadingMsg')
        });
        this.getRecordByIdAndRunView(
            'ticket.TicketUpdateForm',
            params.id,
            TroubleTicket.util.i18n.translate('appTitle.edit')
        );

        return this;
    },
    /**
     * It is the view ticket action which can be routed by the Ext.ux.Router. This will run the view ticket view
     * via the application {@link TroubleTicket.Application#runView runView} method.
     * @returns {TroubleTicket.controller.Ticket} this
     */
    actionView: function (params) {
        "use strict";

        this.getRecordByIdAndRunView(
            'ticket.TicketView', // view
            params.id, // id for record
            TroubleTicket.util.i18n.translate('appTitle.view'), // title
            { // path
                token: 'ticket/list?sourceApplication='+this._sourceApplication,
                label: TroubleTicket.util.i18n.translate('pathLabel.backToList')
            }
        );
        return this;
    },

    /**
     *
     * @returns {Ext.data.Model|*}
     */
    getSelectedTicketId : function() {
        var record = this.getSelectedTicketRecord(),
            id = record && record.getData()[record.idProperty];

        return id;
    },

    /**
     * Return the record associated to the ticket that is displayed
     *
     * @returns {Ext.data.Model|*}
     */
    getSelectedTicketRecord : function() {
        var formPanel = this.getTicketFormPanel(),
            form = formPanel ? formPanel.getForm() : null,
            record = form ? form.getRecord() : null;

        return record;
    },

    /**
     * Method remove an attachment remotely through an ajax call to the server side.
     * SuccessCallback represents the action to execute if the deleted is done successfully. It is possible to pass
     * the scope that the successCallback function will use.
     *
     * @param file
     */
    deleteAttachment : function(file, successCallback, scope){

        var url = '/troubleticketapi/api/v1/tickets/'+this.getSelectedTicketId()+'/attachments/'+file.id
            +'?sourceApplication='+ this._sourceApplication;

        TroubleTicket.app.viewport.setLoading(true);

        Ext.Ajax.request({
            url: url,
            method: 'DELETE',
            success: function(response) {
                //Attachment removed successfully
                if(successCallback) {
                    successCallback.call(scope);
                }
                TroubleTicket.app.viewport.setLoading(false);
            },
            failure: function(response) {
                var msg = {title: TroubleTicket.util.i18n.translate('updateTicket.attachments.errorDeleting'),
                    msg: JSON.parse(response.responseText)};

                TroubleTicket.getApplication().fireEvent('showMessage', msg, function () {
                    TroubleTicket.app.viewport.setLoading(false);
                });
            }
        }, this);
    }
});