/**
 * Created by david on 22/01/2014.
 */
Ext.define('TroubleTicket.controller.MessageController', {
    extend: 'Ext.app.Controller',

    /**
     * Initialize the controller: add the handlers for the form panel
     * @return {TroubleTicket.controller.MessageController}
     */
    messageBox: undefined,

    init: function () {
        "use strict";

        this.application.on({
            showMessage: this.showMessage,
            errorHandler: this.serverErrorHandler,
            labelErrorHandler: this.labelErrorHandler,
            scope: this
        });

        return this;
    },

    /**
     * Create a popup msg with a title and a message
     * @param msg
     * - msg: the message for the operation result
     * - title: the title for the operation result
     * @param callbackFn call to the callbackFn if it is defined
     * @returns {Object}
     */
    showMessage: function (msg, callbackFn) {

        //TP-34674 Hide the loading mask.
        TroubleTicket.getApplication().getViewport().setLoading(false);

        Ext.Msg.show({
                title: msg.title,
                msg: msg.msg,
                buttonAlign : 'right',
                buttonText: {yes: TroubleTicket.util.i18n.translate('messageWindow.success.buttonText')},
                fn: function(btn){              
                    if(btn === 'yes' && TroubleTicket.util.Util.isFunction(callbackFn)) {
                       callbackFn();
                    }
                },
                modal: true,
                shadow: false,
                closable: false
            }
        );
    },

    /**
     * Create a popup msg with errors returned from store load
     * - msg: the message for the operation result
     * - title: the title for the operation result
     * @param {Ext.data.Operation} operation
     * @param {Object} [records] The data object to apply to the message
     * @param {Object} [success] boolean
     * @param {Object} [store] passed store
     * @returns {Object}
     */
    errorHandler : function (records, operation, success, store) {
        var msg={},
            errors

        if (operation.serverResponse.status < 399) {

            if (operation.serverResponse.validationErrors) {
                // Anything less than 499 should have a JSON response
                // If there are validation errors, deal with them
                msg.title = operation.serverResponse.message;
                msg.msg = operation.serverResponse.validationErrors[0].message;
            } else {
                msgData = store.getCount() > 0 ? store.data : {};
                if (errors && errors.length) {
                    msgData.errorList = record.getErrorListHtml();
                }
                msg = TroubleTicket.util.i18n.getOperationMsgData(operation, msgData, {
                    msg: 'Error while loading item.<br/>__errorList__',
                    title: 'Error'
                });
            }
        }
        this.getApplication().fireEvent('showMessage', msg);
    },

    /**
     * Create a popup msg with errors returned from store load
     * - msg: the message for the operation result
     * - title: the title for the operation result
     * @param {Object} Server response with error message and status code
     * @returns {Object}
     */
    labelErrorHandler : function (records, operation, success, store) {
        var msg={},
            errors = [];

        //Not Client or server errors
        if(!TroubleTicket.util.Util.isClientServerRequestError(operation.serverResponse.status)) {

            if (operation.serverResponse.validationErrors) {
                // Anything less than 399 should have a JSON response
                // If there are validation errors, deal with them
                msg.title = operation.serverResponse.message;
                msg.msg = operation.serverResponse.validationErrors[0].message;
            } else {
                var msgData = store.getCount() > 0 ? store.data : {};
                if (errors && errors.length) {
                    msgData.errorList = record.getErrorListHtml();
                }
                msg = TroubleTicket.util.i18n.getOperationMsgData(operation, msgData, {
                    msg: 'Error while loading item.<br/>__errorList__',
                    title: 'Error'
                });
            }

            this.getApplication().fireEvent('showMessage', msg);
        }
    },

    /**
     * Create a popup msg with errors returned from the http response when a 400 or 500 error are thown
     * @param {Object} Server response with error message and status code
     * @returns {Object}
     */
    serverErrorHandler : function (response) {
        var msg={},
            jsonResponse = null;

        try {
            jsonResponse = JSON.parse(response.responseText);
        } catch (err) {

        }
        //Client or server errors
        if(TroubleTicket.util.Util.isClientServerRequestError(response.status)) {

            if (jsonResponse && jsonResponse.errorCode) {
                // Check HTTP error first
                msg.title = response.status;
                msg.msg = TroubleTicket.vars.config.labels.errorCodes[''+jsonResponse.errorCode];
            } else {
                msg.title = response.status;
                msg.msg = response.statusText;
            }

            this.getApplication().fireEvent('showMessage', msg);
        }
    }
});
