Ext.define('TroubleTicket.config.view.ticketList', {
    statics : {
        // maximum number of days to allow on 'From' date for custom date selection
        dateLimit: 180,
        submitFormat : 'Y-m-d',
        format : 'd M Y',
        defaultTimePeriod : 'last30Days',
        defaultSearchType : 'imsi'
    }
});