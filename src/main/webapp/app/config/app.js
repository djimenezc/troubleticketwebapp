Ext.define('TroubleTicket.config.app', {
    statics : {
        debug: true,
        locale: 'en',
        dateTimeFormat: 'd M Y H:i',
        dateFormat: 'd M Y',
        timeFormat: 'H:i',
        labelDefault : 'Labels not available',
        layout: {
            'ticket/add': false,
            'ticket/open': false
        }
    }
});