Ext.define('TroubleTicket.config.store.tickets', {
    statics : {
        proxy: {
            type: 'rest',
            url: '/troubleticketapi/ticket'
        }
    }
});