Ext.define('TroubleTicket.config.model.ticket', {
    statics : {
        proxy: {
            type: 'rest',
            url: '/troubleticketapi/api/v1/tickets',
            reader: 'dynamicjson',
            extraParams: {
                sourceApplication: TroubleTicket.util.Util.getQueryParameter('sourceApplication')
            }
        },
        apiDateFormat: 'Y-m-dTH:i:s',
        attachmentLimit: 5
    }
});