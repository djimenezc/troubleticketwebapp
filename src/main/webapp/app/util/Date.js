/**
 * This class if a date/datetime/time utility to format it.
 * @class TroubleTicket.util.Date
 * @singleton
 */
(function () {
    Ext.ns('TroubleTicket.util');
    var dateUtil = TroubleTicket.util.Date = {};
    Ext.apply(dateUtil, {
        sourceDateFormat: 'Y-m-d',
        sourceTimeFormat: ['H:i', 'H:i:s'],
        sourceDateTimeFormat: ['c'], // ISO 8601 date

        serverDateFormat : ['Y-m-d\\TH:i:s'],
        serverDatePattern : /^\d{4}-\d{1,2}-\d{1,2}T\d{1,2}:\d{1,2}:\d{1,2}$/,
        /**
         * Format the given date/time/datetime corresponding the arguments or the configuration
         * @param {String/Date} date The date string or the date object. If this parameter is String than the
         * sourceFormat argument or configuration property will be used to recognize and build the Date object
         * @param {String} [outputFormat] The format of the output date(time) string
         * @param {String} [sourceFormat] The format of the given string represented date
         * @returns {String}
         */
        format: function (date, outputFormat, sourceFormat) {
            "use strict";
            var cfg = Ext.ns('TroubleTicket.config.app');
            outputFormat = Ext.isString(outputFormat) ? outputFormat : cfg.dateFormat;
            sourceFormat = Ext.isObject(sourceFormat) || Ext.isArray(sourceFormat)
                ? sourceFormat : dateUtil.sourceDateFormat;
            if (Ext.isString(date)) {
                if (Ext.isString(sourceFormat)) {
                    sourceFormat = [sourceFormat];
                }
                Ext.each(sourceFormat, function (format) {
                    var d = Ext.Date.parse(date, format);
                    if (d) {
                        date = d;
                    }
                    return !d;
                });
            }
            return Ext.Date.format(date, outputFormat);
        },
        /**
         * Format the given date corresponding the arguments or the configuration.
         * @param {String/Date} date The date string or the date object. If this parameter is String than the
         * sourceFormat argument or configuration property will be used to recognize and build the Date object
         * @param {String} [outputFormat] The format of the output date(time) string
         * @param {String} [sourceFormat] The format of the given string represented date
         * @returns {String}
         */
        formatDate: function (date, outputFormat, sourceFormat) {
            "use strict";
            var cfg = Ext.ns('TroubleTicket.config.app');
            return dateUtil.format(
                date,
                Ext.isString(outputFormat) ? outputFormat : cfg.dateFormat,
                Ext.isString(sourceFormat) ? sourceFormat : dateUtil.sourceDateFormat
            );
        },
        /**
         * Format the given time corresponding the arguments or the configuration.
         * @param {String/Date} date The date string or the date object. If this parameter is String than the
         * sourceFormat argument or configuration property will be used to recognize and build the Date object
         * @param {String} [outputFormat] The format of the output date(time) string
         * @param {String} [sourceFormat] The format of the given string represented date
         * @returns {String}
         */
        formatTime: function (date, outputFormat, sourceFormat) {
            "use strict";
            var cfg = Ext.ns('TroubleTicket.config.app');
            return dateUtil.format(
                date,
                Ext.isString(outputFormat) ? outputFormat : cfg.timeFormat,
                Ext.isString(sourceFormat) ? sourceFormat : dateUtil.sourceTimeFormat
            );
        },
        /**
         * Format the given datetime corresponding the arguments or the configuration.
         * @param {String/Date} date The date string or the date object. If this parameter is String than the
         * sourceFormat argument or configuration property will be used to recognize and build the Date object
         * @param {String} [outputFormat] The format of the output date(time) string
         * @param {String} [sourceFormat] The format of the given string represented date
         * @returns {String}
         */
        formatDateTime: function (date, outputFormat, sourceFormat) {
            "use strict";
            var cfg = Ext.ns('TroubleTicket.config.app');
            return dateUtil.format(
                date,
                Ext.isString(outputFormat) ? outputFormat : cfg.dateTimeFormat,
                Ext.isString(sourceFormat) ? sourceFormat : dateUtil.sourceDateTimeFormat
            );
        },

        /**
         * Format the given string to a date format if the value match with the expression yyyy-MM-ddThh:mm:ssZ.
         * If the string doesn't match with the pattern the value returned will be the original.
         * @param {String} value The date string to format.
         * @returns {String}
         */
        applyDefaultDateFormat: function (value) {
            "use strict";
            var result = value;

            // if the value match with a date expression with this format 2014-01-08T12:05:14Z
            // the value return is parsed using the sourceFormat pattern
            if (dateUtil.serverDatePattern.test(value)) {

                var outputFormat = {};

                result = dateUtil.formatDateTime(value, outputFormat, dateUtil.serverDateFormat);
            }

            return result;
        }
    });
}());