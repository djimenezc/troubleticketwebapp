Ext.define('TroubleTicket.util.String', {
    singleton: true,
    /**
     * Replace the new line characters to <br/>.
     * @param {String} str
     * @returns {String}
     */
    newLineToBr: function (str) {
        "use strict";
        return (str || '').replace(/\n/g, '<br/>');
    }
});