/**
 * Created by nickrogers on 21/01/2014.
 */
Ext.define('TroubleTicket.util.i18n', {
    singleton: true,
    /**
     * Our locale translator. Receives a string and checks for corresponding object
     * @param {String} String to be translated. In format 'xxxx.xxxx.xxxx'. We loop through the nodes and return if found.
     * @returns {String} Translated string or default label missing message
     */
    translate : function(translation, data, defaultLabel) {
        "use strict";
        var loc = TroubleTicket.vars.config.labels,
            key = translation.split('.'),
            l = loc;
        Ext.each(key, function (node, index) {
            if (l[node]) {
                l = l[node];
            } else {
                return l = TroubleTicket.config.app.labelDefault;
            }
        });
        if (data && l.indexOf('__') !== -1) {
            l = this.parseDataIntoMsg(l, data);
        }
        //If the label wasn't found and the defaultLabel is present, that will be returned
        if(defaultLabel && l === TroubleTicket.config.app.labelDefault) {
            l= defaultLabel;
        }
        return(l);
    },
    parseDataIntoMsg : function (label, data) {
        "use strict";
        var x,
            parsedLabel = label;
        for (x in data) {
            if (data.hasOwnProperty(x)) {
                parsedLabel = parsedLabel.replace("__"+x+"__", data[x]);
            }
        }
        return parsedLabel;
    },

    /**
     * Returns with an object with two property:
     * - msg: the message for the operation result
     * - title: the title for the operation result
     * @param {Ext.data.Operation} operation
     * @param {Object} [data] The data object to apply to the message
     * @param {Object} [defaults]
     * @returns {Object}
     */
    getOperationMsgData: function (operation, data, defaults) {
        "use strict";
        var res = defaults || {},
            error,
            action,
            success;
        if (operation && operation.action) {
            error = operation.error;
            action = operation.action;
            success = operation.success;

            res.msg = TroubleTicket.util.i18n.translate(action + 'Ticket.' + (success ? 'success' : 'failure') +
                (error && error.status ? '.' + error.status : '') + '.msg', data);

            res.title = TroubleTicket.util.i18n.translate(action + 'Ticket.'  + (success ? 'success' : 'failure') +
                (error && error.status ? '.' + error.status : '') +
                '.title');

        }
        return res;
    },
});