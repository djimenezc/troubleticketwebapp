Ext.define('TroubleTicket.util.Util', {
    singleton: true,
    /**
     * Return true if object is a function
     * @param {Object} object
     * @returns {Boolean}
     */
    isFunction: function(object) {
     return object && object instanceof Function;
    },

    /**
     * Return the query parameter value for the query parameter name
     * If the query parameter is not present in the URL undefined will be returned
     *
     * @param name
     * @return [String]
     */
    getQueryParameter : function(name) {

        var queryParameters = this.getQueryParameters();

        return queryParameters[name];
    },

    /**
     * Return the query parameter currently present in the url
     *
     * @returns {{}}
     */
    getQueryParameters : function () {
        "use strict";
        var queryObject = {},
            queryString = document.location.href.split('?')[1];
        // If there is a query string, we are coming from an external application
        if (queryString) {
            queryObject = Ext.Object.fromQueryString(queryString);
        }

        return queryObject;
    },

    /**
     * Return true if the error code correspond to a client or server http error
     * @param code
     * @returns {boolean}
     */
    isClientServerRequestError : function(code) {
        return code >= 399 || code === 0;
    }

});
