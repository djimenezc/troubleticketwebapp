/**
 * The model class for the attachment files of a {@link TroubleTicket.model.ticket.Ticket Ticket}
 * @class TroubleTicket.model.ticket.Attachment
 */
Ext.define('TroubleTicket.model.ticket.Attachment', {
    extend: 'Ext.data.Model',
    alias: 'widget.attachmentModel',
    requires: ['Ext.data.proxy.Rest'],
    fields: [
        { name: 'filename', type: 'string' },
        { name: 'mimetype', type: 'string' },
        { name: 'url', type: 'string' },
        { name: 'base64Data', type: 'string' },
        { name: 'contentURL', type: 'string' }
    ],
    associations: [{
        type: 'belongsTo',
        getterName: 'getTicket',
        model: 'TroubleTicket.model.ticket.Ticket'
    }],
    /**
     * Override because if there is not url provided by the server, we create one. It will create URL only when the
     * record is not phantom (already exists on the server)
     * @returns {Object}
     */
    getData: function () {
        var data = this.callParent(arguments);
        if (!data.url && !this.phantom) {
            data.url = this.getAutoUrl();
        }
        return data;
    },
    /**
     * Will return with the API download url for the attachment
     * @returns {string}
     */
    getAutoUrl: function () {
        "use strict";
        var ticket = this.getTicket(),
            url = '';
        if (!ticket.phantom && this.get('id')) {
            url = ticket.getProxy().url + '/' + ticket.get('id') + '/attachments/'
                + this.get('id') + '/' + this.get('filename');
        }
        return url;
    }
});
