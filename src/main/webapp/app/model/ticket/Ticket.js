/**
 * @class TroubleTicket.model.ticket.Ticket
 * The model class for the ticket data. It is a flexible class it will add any custom fields to the fields list if need.
 */
Ext.define('TroubleTicket.model.ticket.Ticket', {
    extend: 'Ext.data.Model',
    alias: 'widget.baseTicketModel',
    requires: [
        'TroubleTicket.config.model.ticket',
        'TroubleTicket.model.ticket.Attachment',
        'TroubleTicket.model.ticket.Comment',
        'Ext.overrides.data.writer.Writer',
        'Ext.overrides.data.proxy.ServerOverride',
        'Ext.data.proxy.Rest'
    ],
    fields: [
        {name: 'title', type: 'string'},
        {name: 'startTime', type: 'time'},
        {name: 'endTime', type: 'time'},
        {name: 'title', type: 'string'},
        {name: 'location', type: 'string'},
        {name: 'description', type: 'string'},
        {name: 'sourceApplication', type: 'string'},
        {name: 'imsi', type: 'string'},
        {name: 'msisdn', type: 'string'},
        {name: 'status', type: 'string'},
        {name: 'createdBy', type: 'string'}
    ],
    validations: [
        {type: 'presence', field: 'sourceApplication'},
        {type: 'format', field: 'description', matcher: /\w/},
        {type: 'format', field: 'title', matcher: /\w/},
        {type: 'format', field: 'startTime', matcher: /^([\+-]?\d{4}(?!\d{2}\b))((-?)((0[1-9]|1[0-2])(\3([12]\d|0[1-9]|3[01]))?|W([0-4]\d|5[0-2])(-?[1-7])?|(00[1-9]|0[1-9]\d|[12]\d{2}|3([0-5]\d|6[1-6])))([T\s]((([01]\d|2[0-3])((:?)[0-5]\d)?|24\:?00)([\.,]\d+(?!:))?)?(\17[0-5]\d([\.,]\d+)?)?([zZ]|([\+-])([01]\d|2[0-3]):?([0-5]\d)?)?)?)?$/},
        {type: 'format', field: 'endTime', matcher: /^([\+-]?\d{4}(?!\d{2}\b))((-?)((0[1-9]|1[0-2])(\3([12]\d|0[1-9]|3[01]))?|W([0-4]\d|5[0-2])(-?[1-7])?|(00[1-9]|0[1-9]\d|[12]\d{2}|3([0-5]\d|6[1-6])))([T\s]((([01]\d|2[0-3])((:?)[0-5]\d)?|24\:?00)([\.,]\d+(?!:))?)?(\17[0-5]\d([\.,]\d+)?)?([zZ]|([\+-])([01]\d|2[0-3]):?([0-5]\d)?)?)?)?$/}
    ],
    customFieldValidations: [
        {type: 'format', field: 'imsi', matcher: /^(|\d{15})$/},
        {type: 'format', field: 'msisdn', matcher: /^(|\d{1,128})$/}
    ],
    associations: [{
        type: 'hasMany',
        model: 'TroubleTicket.model.ticket.Attachment',
        name: 'attachments',
        associationKey: 'attachments'
    }, {
        type: 'hasMany',
        model: 'TroubleTicket.model.ticket.Comment',
        name: 'comments',
        associationKey: 'comments'
    }],
    inheritableStatics: {
        /**
         * Returns the configured Proxy for this Model
         * @return {Ext.data.proxy.Proxy} The proxy
         * @static
         * @inheritable
         */
        getProxy: function () {
            "use strict";
            var proxy = this.proxy;
            // Not yet been created wither from prototype property set in onClassExtended, or by cloning superclass's Proxy...
            if (!proxy) {
                proxy = this.setProxy(Ext.ns('TroubleTicket.config.model.ticket.proxy'));
            }
            return proxy;
        }
    },
    /**
     * @constructor
     * @param {Object} data
     */
    constructor: function (data, id, raw, convertedData) {
        var fields = this.fields,
            d = data || raw,
            reader;
        // remove the key fields which has no value, so we don't send them to the server
        if (!d.msisdn) {
            fields.removeAt(fields.indexOfKey('msisdn'));
        }
        if (!d.imsi) {
            fields.removeAt(fields.indexOfKey('imsi'));
        }
        if (Ext.isObject(d)) {
            Ext.Object.each(d, this.addDataFieldIfNotExists, this);
        }
        this.callParent(arguments);
        // when the model has been created just by calling the constructor with (raw)data it means the associated
        // data has not been setup, but we need it, so we use the original Ext way to set it via the reader.
        if (data) {
            reader = this.getProxy() && this.getProxy().getReader();
            if (reader) {
                reader.readAssociated(this, data);
            }
        }
    },
    /**
     * Returns with the config object for the Ticket model
     * @returns {Object}
     */
    getCfg: function () {
        "use strict";
        return Ext.ns('TroubleTicket.config.model.ticket');
    },
    /**
     * Add a field to the fields collection if the field does not exist yet.
     * @private
     */
    addDataFieldIfNotExists: function (key) {
        "use strict";
        var fields = this.fields,
            field = fields.getByKey(key),
            associateField = this.associations.findBy(function (item) {
                return item.name === key;
            });
        if (!field && !associateField) {
            field = new Ext.data.Field({
                name: key
            });
            fields.add(field);
        }
        return field;
    },
    /**
     * Validate the date-time field with Ext.Date.isValid method
     * @param {String} field The name of the field
     * @returns {boolean}
     */
    validateDateTimeField: function (field) {
        "use strict";
        var res = true;
        if (!Ext.Date.isValid.apply(Ext.Date, this.get(field).split(/\s|\:|\-|T/))) {
            res = {
                field: field,
                message: TroubleTicket.util.i18n.translate('validationError.' + field)
            };
        }
        return res;
    },
    /**
     * Validates the custom fields after the customFieldValidations. So if the field doesn't exist we just skip the
     * validation.
     * @param {Ext.data.Errors} errors Optional error object
     * @returns {Ext.data.Errors}
     */
    validateCustomFields: function (errors) {
        "use strict";
        var validations = this.customFieldValidations,
            validators  = Ext.data.validations,
            length,
            validation,
            field,
            valid,
            type,
            i;
        errors = errors || new Ext.data.Errors();
        if (validations) {
            length = validations.length;
            for (i = 0; i < length; i += 1) {
                validation = validations[i];
                field = validation.field || validation.name;
                if (this.fields.indexOfKey(field) > -1) {
                    type  = validation.type;
                    valid = validators[type](validation, this.get(field));
                    if (!valid) {
                        errors.add({
                            field  : field,
                            message: validation.message || validators[type + 'Message']
                        });
                    }
                }
            }
        }
        return errors;
    },
    /**
     * This method is overriden because we need some special validation for the custom fields and better validation for
     * the datetime
     * @param {Boolean} validateForSaving If it is false will skipp the validation of the title. It needs when we
     * validate an incoming ticket from a source application from where we don't get title, but the title itself is
     * required in the ticketing system.
     * @returns {Ext.data.Errors}
     */
    validate: function (validateForSaving) {
        var res = this.callParent(arguments),
            startTimeIsValid = !res.getByField('startTime').length,
            endTimeIsValid = !res.getByField('endTime').length,
            attachmentValidation = this.validateAttachments(),
            err;
        if (validateForSaving === false) {
            res.removeAt(res.findIndexBy(function (err) {
                return err.field === 'title';
            }));
        }
        if (startTimeIsValid) {
            err = this.validateDateTimeField('startTime');
            if (err !== true) {
                res.add(err);
            }
        }
        if (endTimeIsValid) {
            err = this.validateDateTimeField('endTime');
            if (err !== true) {
                res.add(err);
            }
        }
        if (startTimeIsValid && endTimeIsValid && this.get('startTime') > this.get('endTime')) {
            res.add({
                field: 'endTime',
                message: TroubleTicket.util.i18n.translate('validationError.invalidStartEndTime')
            });
        }
        if (attachmentValidation !== true) {
            res.add(attachmentValidation);
        }
        if (!this.get('msisdn') && !this.get('imsi')) {
            res.add({
                field: 'imsi msisdn',
                message: TroubleTicket.util.i18n.translate('validationError.noImsiNoMsisdn')
            });
        }
        this.validateCustomFields(res);
        return res;
    },
    /**
     * Validate the associated attachments. Check the total attachments number to compare with the max total.
     * @returns {boolean}
     */
    validateAttachments: function () {
        "use strict";
        var cfg = this.getCfg(),
            store = this.attachments ? this.attachments() : null,
            res = true;
        if (store && store.data.items.length > cfg.attachmentLimit) {
            res = {
                field: 'attachments',
                message: TroubleTicket.util.i18n.translate('validationError.tooManyAttachment', {
                    limit: cfg.attachmentLimit
                })
            };
        }
        return res;
    },
    /**
     * Returns with a html list of the errors
     * @param {Boolean} validateForSaving If it is false will skipp the validation of the title. It needs when we
     * validate an incoming ticket from a source application from where we don't get title, but the title itself is
     * required in the ticketing system.
     * @returns {String} The unordered html list with the raised errors.
     */
    getErrorListHtml: function (validateForSaving) {
        "use strict";
        var errors = this.validate(validateForSaving),
            msg = '';
        if (!errors.isValid()) {
            msg = '<ul>';
            errors.each(function (e) {
                var fieldLabel = e.field;
                msg += '<li>' +
                        TroubleTicket.util.i18n.translate('validationError.' + e.field,null, e.message) +
                    '</li>';
            }, this);
            msg += '</ul>';
        }
        return msg;
    },
    statics: {
        /**
         * Return with a class for the given ticket data, it depends on the 'sourceApplication' property. If the model
         * class for TroubleTicket.model.ticket.{rec.sourceApplication}Ticket namespace exists then it returns with that,
         * or with TroubleTicket.model.ticket.Ticket class otherwise.
         * @param {Object} rec An object with key value pair.
         * @returns {TroubleTicket.model.ticket.Ticket}
         */
        getModelFromTicketRecord: function (rec) {
            "use strict";
            var modelNs = this.$className,
                modelNsFragments = modelNs.split('.'),
                model,
                fields,
                associations;
            if (rec.sourceApplication) { // create a new model with application context
                modelNsFragments[modelNsFragments.length - 1] = rec.sourceApplication +
                    modelNsFragments[modelNsFragments.length - 1];
                modelNs = modelNsFragments.join('.');
                model = Ext.ns(modelNs);
            } else { // create a new model with class random name
                modelNs = null;
            }
            if (!modelNs || !model || Ext.Object.isEmpty(model)) {
                fields = this.prototype.fields.clone();
                associations = this.prototype.associations;
                Ext.Object.each(rec, function (name) {
                    var field;
                    if (!fields.getByKey(name) && !associations.findBy(function (item) {
                            return item.name === name;
                        })) {
                        field = new Ext.data.Field({
                            name: name
                        });
                        fields.add(field);
                    }
                });
                model = Ext.define(modelNs, {
                    extend: this.$className,
                    fields: fields.items
                });
                //model = Ext.ns(modelNs);
            }
            return !model || Ext.Object.isEmpty(model) ? this : model;
        },
        /**
         * Create a Ticket model instance from a ticket object converted from a ticket description object server
         * response.
         * @param {Object} rec This is a key value pair object, not the description object!
         * @returns {TroubleTicket.model.ticket.Ticket}
         */
        createFromTicketRecord: function (rec) {
            "use strict";
            var Model = this.getModelFromTicketRecord(rec);
            return new Model(rec);
        }
    }
});
