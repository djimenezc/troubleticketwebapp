/**
 * The model class for the comments of of a {@link TroubleTicket.model.ticket.Ticket Ticket}
 * @class TroubleTicket.model.ticket.Comment
 */
Ext.define('TroubleTicket.model.ticket.Comment', {
    extend: 'Ext.data.Model',
    alias: 'widget.commentModel',
    requires: ['Ext.data.proxy.Rest'],
    fields: [
        { name: 'description', type: 'string' },
        { name: 'created', type: 'string' },
        { name: 'createdBy', type: 'string' }
    ],
    associations: [{
        type: 'belongsTo',
        model: 'TroubleTicket.model.ticket.Ticket',
        name: 'ticket'
    }]
});
