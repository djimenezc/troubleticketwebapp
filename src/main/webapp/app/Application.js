/**
 * This is the application class. It will initialize the Router and the layout.
 * @class TroubleTicket.Application
 */
Ext.define('TroubleTicket.Application', {
    name: 'TroubleTicket',
    extend: 'Ext.app.Application',
    views: [
        'shared.PageHeader',
        'shared.AppHeader',
        'shared.PageContent',
        'shared.AppContent',
        'shared.PageFooter',
        'shared.AppFooter',
        'Viewport',
        'Main'
    ],
    controllers: [
        'Ticket',
        'TicketList',
        'MessageController'
    ],
    requires: [
        'Ext.ux.Router',
        'TroubleTicket.config.app',
        'TroubleTicket.util.i18n',
        'TroubleTicket.util.Date',
        'TroubleTicket.util.Util'
    ],
    /**
     * This property is used by the Ext.ux.Router to determine the available routes. In the {@link #launch} method we
     * collect all the routes from the controllers. https://github.com/brunotavares/Ext.ux.Router/
     * @private
     */
    routes: {
        '/' : 'ticket#actionOpen'
    },
    /**
     * @cfg {String} viewportSelector
     * The viewport's ComponentQuery selector
     */
    viewportSelector: '.viewport',
    /**
     * @cfg {String} appContentCtrSelector
     * The application content container's ComponentQuery selector
     */
    appContentCtrSelector: '.appContent',
    /**
     * @cfg {String} appHeaderCtrSelector
     * The application content container's ComponentQuery selector
     */
    appHeaderCtrSelector: '.appHeader',
    /**
     * @cfg {String} appTitleCtrSelector
     * The application content container's ComponentQuery selector
     */
    appTitleCtrSelector: '.appTitle',
    /**
     * @cfg {String} appBreadcrumbCtrSelector
     * The application breadcrumbs container's ComponentQuery selector
     */
    appBreadcrumbCtrSelector: '.appBreadcrumb',
    /**
     * @cfg {String} defaultAppLayout
     * The default layout for the application
     * @type {String|Object} It can be a string for xtype or an object for CT config.
     */
    defaultAppLayout: 'app-main',
    /**
     * Cache for the viewport
     * @private
     */
    viewport: null,
    /**
     * The currently used layout name
     * @private
     */
    currentAppLayout: null,
    /**
     * Init the application, add the new events.
     */
    init: function () {
        "use strict";
        var cfg = Ext.ns('TroubleTicket.config.app');
        this.setVarsConfig();
        if (cfg.locale) {
            Ext.Loader.loadScriptFile(
                Ext.Loader.getPath('Ext.locale.ext-lang-' + cfg.locale.replace('-', '_')),
                Ext.identityFn,
                Ext.identityFn,
                this,
                true
            );
        }
        this.addEvents(
            'beforeRunView',
            'runView',
            'afterlaunch'
        );
        if (Ext.isString(this.defaultAppLayout)) {
            this.defaultAppLayout = {xtype: this.defaultAppLayout};
        }
    },
    /**
     * Initialize the i18next library
     * @private
     */
    onBeforeLaunch: function () {
        this.initRoutes();
        // Ext.onReady already called (because ext-all used) - the router needs to initialize
        Ext.Router.on('routemissed', this.onRouteMissed, this);
        if (Ext.isReady) {
            if (!Ext.ux.Router.ready) {
                Ext.Router.init(this);
            }
            Ext.Router.parse(Ext.History.getToken() || '');
        }
        return this.callParent(arguments);
    },
    /**
     * Launch the application. If we have a delayed view to display try to run it.
     */
    launch: function () {
        "use strict";
        
        //don't set loading if coming from '#ticket/add' TEMPORARY FIX - THIS NEEDS TO BE REFACTORED SO MASKING IS NOT CONTROLLED FROM HERE
        if(document.URL.indexOf('#ticket/add') === -1){
        	this.viewport.setLoading(true);
        }
        
        var view = this._viewOnLaunch,
            token;

        if (view) {
            this.runView(this._viewOnLaunch);
            delete this._viewOnLaunch;
        } else {
            // This piece of the code is because of the test now, it will fail if the vieport initialized twice
            token = Ext.History.getToken();
            if (!token) {
                Ext.Router.parse('');
            }
        }

        this.fireEvent('afterlaunch', this);
    },
    /**
     * Apply all the available configuration from the TroubleTicket.vars.config to the TroubleTicket.config if it is
     * not defined there yet.
     * @protected
     */
    setVarsConfig: function () {
        "use strict";
        if (TroubleTicket.vars && TroubleTicket.vars.config) {
            Ext.applyIf(TroubleTicket.config, TroubleTicket.vars.config);
        }
    },
    /**
     * Returns with the application layout which corresponds to the given or current token
     * @param {String} [token] The token which needs a layout
     * @return {String} The xtype of the layout
     * @return {Object} The configuration for the layout
     */
    getAppLayoutByToken: function (token) {
        "use strict";
        var cfg = Ext.ns('TroubleTicket.config.app'),
            layout;
        token = token || Ext.History.getToken();
        layout = cfg.layout[token];
        return typeof layout === 'undefined' ? this.defaultAppLayout : layout;
    },
    /**
     * Initialize the viewport and add the layout if any
     * @param {String/Object} [layout] The xtype of the layout or the config object of the layout.
     * @returns {Ext.container.Viewport}
     */
    initViewport: function (layout) {
        "use strict";
        if (!this.viewport) {
            this.viewport = this.getView('Viewport').create();
        }
        layout = typeof layout === 'undefined' ? this.getAppLayoutByToken() : layout;
        if (typeof layout === 'string') {
            layout = {xtype: layout};
        }
        if (this.viewport && layout !== this.currentAppLayout) {
            this.currentAppLayout = layout;
            this.viewport.removeAll(true);
            if (layout) {
                this.viewport.add(layout);
            }
        }
        return this.viewport;
    },
    /**
     * Return with the application config
     * @returns {Object} The configuration object if it has any.
     */
    getAppCfg: function () {
        "use strict";
        return Ext.ns('TroubleTicket.config.app');
    },
    /**
     * It iterate all the controllers and initialize their routes.
     * @returns {TroubleTicket.Application} this
     */
    initRoutes: function () {
        "use strict";
        this.controllers.each(this.initControllerRoutes, this);
        return this;
    },
    /**
     * Read the routes from the controller and add it to the routes property via {@link #addRoutes} method.
     * @param {Ext.app.Controller} ctr Controller instance
     * @returns {TroubleTicket.Application} this
     */
    initControllerRoutes: function (ctr) {
        "use strict";
        if (ctr && ctr.routes) {
            this.addRoutes(ctr.routes);
        }
        return this;
    },
    /**
     * Add the given routes to the application for the Ext.ux.Route router.
     * @param {Object} routes
     * @returns {TroubleTicket.Application}
     */
    addRoutes: function (routes) {
        "use strict";
        this.routes = this.routes || {};
        Ext.apply(this.routes, routes);
        return this;
    },
    /**
     * Returns with the viewport of the application.
     * @returns {Ext.Component}
     */
    getViewport: function () {
        "use strict";
        if (!this.viewport) {
            this.initViewport();
        }
        return this.viewport;
    },
    /**
     * Try to find the application header container panel and returns with.
     * @returns {Ext.Component}
     */
    getAppHeaderContainer: function () {
        "use strict";
        return this.getViewport().down(this.appHeaderCtrSelector) || null;
    },
    /**
     * Try to find the application title container panel and returns with.
     * @returns {Ext.Component}
     */
    getAppTitleContainer: function () {
        "use strict";
        var viewport = this.getViewport(),
            ctr;
        if (!this.currentAppLayout) {
            ctr = viewport.items.getAt(0);
        } else {
            ctr = viewport.down(this.appTitleCtrSelector);
        }
        return ctr || null;
    },
    /**
     * Try to find the application content container panel and returns with.
     * @returns {Ext.Component}
     */
    getAppContentContainer: function () {
        "use strict";
        var viewport = this.getViewport(),
            ctr = viewport.down(this.appContentCtrSelector);
        if (!ctr) {
            ctr = viewport;
        }
        return ctr;
    },
    /**
     * Returns with the component of the app breadcrumb.
     * @return {Ext.Component} The component which handle the breadcrumb.
     * @return {null} If the component doesn't exist in the viewport.
     */
    getBreadcrumbCt: function () {
        "use strict";
        var viewport = this.getViewport(),
            ctr = null;
        if (viewport) {
            ctr = viewport.down(this.appBreadcrumbCtrSelector);
        }
        return ctr;
    },
    /**
     * Event handler for the Ext.ux.Router's 'routemissed' event to show a 'not found' message and redirect to the root.
     * @private
     * @param {String} token
     */
    onRouteMissed: function (token) {
        "use strict";

        var callbackFn = function() {
            Ext.Router.redirect('');
        };
        var msg = {
            title: this._t('route.missing.title'),
            msg : this._t('route.missing.msg', {token: token})
        };
        this.getApplication().fireEvent('showMessage', msg, callbackFn);
    },
/**
     * Run the given view, which means it will be added to the appContent container.
     * @param view {Ext.Component|Object} The view component (eg.: panel) or a config for this component.
     * @param appLayout {String} The requested layout. Currently we have the simple layout (no header and footer) and
     * the main layout.
     * @return {boolean} False if the container component doesn't exist
     * @return {Ext.Component} The added component
     */
    runView: function (view, appLayout) {
        "use strict";
        var ctr = this.getAppContentContainer(),
            res = !!ctr,
            viewport = this.getViewport(),
            title = '',
            breadcrumbCt;
            
        viewport.setLoading(true);
        
        if (this.fireEvent('beforeRunView', view) !== false) {
            if (ctr) {
                //viewport.setLoading(true);
                appLayout = typeof appLayout !== 'undefined' ? appLayout : this.getAppLayoutByToken();
                if (appLayout !== this.currentAppLayout) {
                    this.initViewport(appLayout);
                    this.currentAppLayout = appLayout;
                    ctr = this.getAppContentContainer();
                } else if (ctr.items.length) {
                    ctr.removeAll(true);
                }
                breadcrumbCt = this.getBreadcrumbCt();
                if (view.title) {
                    title = view.title;
                    if (Ext.isObject(view)) {
                        delete view.title;
                    } else if (view.setTitle) {
                        view.setTitle('');
                    }
                }
                res = ctr.add(view);
                if (breadcrumbCt) {
                    if (view.path) {
                        breadcrumbCt.setPath(view.path);
                    } else {
                        breadcrumbCt.clearPath();
                    }
                }
                viewport.setLoading(false);
                this.setTitle(title);
                this.fireEvent('runView', res);
            } else {
                this._viewOnLaunch = view;
            }
        }
        return res;
    },
    /**
     * Set the application title in the ApplicationHeader.
     * @param {String} title The title of the application
     * @param {String} cls Optional css class for the title element
     */
    setTitle: function (title, cls) {
        "use strict";
        var ctr = this.getAppTitleContainer();
        if (ctr) {
            ctr.setTitle(title, cls);
        }
    }
});
