<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page isELIgnored="false"%>
<%@ page import="java.util.Locale" %>
<%
    //Get the client's Locale
    Locale locale = request.getLocale();
    String language = locale.getLanguage();
    String country = locale.getCountry();
    String localeString = language+"_"+country;
    request.setAttribute("localeString",localeString);
    request.setAttribute("applicationName","cqm");
%>
<!DOCTYPE HTML>
<html>
<head>
    <meta charset="UTF-8">
    <title>TroubleTicket</title>
    <script>
        var TroubleTicket = {
                vars: {
                    post:  <%=request.getParameter("ticketContext")%>,
                    config: <c:import url="http://${pageContext.request.serverName}:${pageContext.request.serverPort}/troubleticketapi/api/v1/variables/${applicationName}/${localeString}"  />
                }
            };
    </script>
    <!-- <x-compile> -->
    <!-- <x-bootstrap> -->
    <link rel="stylesheet" href="bootstrap.css">
    <!-- for IE8 to recognise footer element used on comments field to display the date the comment was entered-->
    <script>document.createElement("footer")</script>
    <!-- end of IE8 fix-->
    <script src="vendor/i18next/i18next-1.7.1.js"></script>
    <script src="ext/ext-all.js"></script>
    <script src="bootstrap.js"></script>
    <!-- </x-bootstrap> -->
    <script src="app.js"></script>
    <!-- </x-compile> -->
</head>
<body></body>
</html>
