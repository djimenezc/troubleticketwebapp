'use strict';
module.exports = function(grunt){
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        sencha_jasmine: {
            options: {
                '--web-security' : false,
                specs: 'app-test/spec/**/*.js',
                extFramework : 'ext',
                extLoaderPaths : {
                    "Ext.overrides" : "overrides",
                    "Ext": "ext/src"
                },
                styles : [
                    "bootstrap.css"
                ],
                vendor : [
                    "vendor/jquery/jquery-1.11.0.min.js",
//                    "vendor/i18next/i18next-1.7.1.js",
                    "app-test.js"
                ],
                template : "_TestTemplate.tmpl",
                junit : {
                    "path": "xmlOutput",
                    "consolidate" : "true"
                }
            },
            your_target: {
                // Target-specific file lists and/or options go here.
            }
        }
    });
    grunt.loadNpmTasks('grunt-sencha-jasmine');
    grunt.registerTask('test', 'sencha_jasmine');
};


