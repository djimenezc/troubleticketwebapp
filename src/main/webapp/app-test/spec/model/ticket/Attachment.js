describe("Attachment model", function () {
    var validFileData  = {
            filename: 'export.pdf',
            mimetype: 'application/pdf',
            base64Data: 'TESTDATA'
        },
        ticketData = {
            "id": "2",
            "customerName": "Bob McBob",
            "imsi": "123456789123456",
            "msisdn": "123456789123456",
            "plan": "Plan 9",
            "businessName": "Bobs Magical Wax Show",
            "startTime": "2013-11-09T00:00",
            "endTime": "2013-11-14T23:59",
            "avgBill": "10.00",
            "type": "My kind",
            "tariff": "A",
            "sourceApplication": "cqm",
            "description": "http://example.com",
            "attachments": []
        },
        ticket,
        attachment;
    beforeEach(function () {
        ticket = Ext.create('TroubleTicket.model.ticket.Ticket', ticketData);
        attachment = Ext.create('TroubleTicket.model.ticket.Attachment', validFileData);
        ticket.attachments().add(attachment);
    });

    it("the filename property should equal with the original data", function () {
        expect(attachment.get('filename')).toEqual(validFileData.filename);
    });
    it("the mimetype property should equal with the original data", function () {
        expect(attachment.get('mimetype')).toEqual(validFileData.mimetype);
    });
    it("the base64Data property should equal with the original data", function () {
        expect(attachment.get('base64Data')).toEqual(validFileData.base64Data);
    });
    it("should set new data", function () {
        attachment.set(Ext.apply({id: 222}, validFileData));
        expect(attachment.get('id')).toEqual(222);
    });
    it("The 'getAutoUrl' should return a valid api download url", function () {
        var url;
        attachment.set('id', '222');
        url = attachment.getAutoUrl();
        expect(typeof url).toEqual('string');
        expect(url.length).toBeGreaterThan(0);
    });
    it("shouldn't generate url when it is new on 'setData' called", function () {
        var data;
        data = attachment.getData();
        expect(data).not.toBeNull();
        expect(data.url).toEqual('');
    });
    it("should generate url when it is not phantom/new on 'setData' called", function () {
        var data;
        attachment.set('id', 222);
        data = attachment.getData();
        expect(data).not.toBeNull();
        expect(data.url).toBeDefined();
        expect(typeof data.url).toEqual('string');
        expect(data.url.length).toBeGreaterThan(0);
    });
});