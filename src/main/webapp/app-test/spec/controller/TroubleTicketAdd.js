(function () {
    var dateValidatorRegexp = /^([\+-]?\d{4}(?!\d{2}\b))((-?)((0[1-9]|1[0-2])(\3([12]\d|0[1-9]|3[01]))?|W([0-4]\d|5[0-2])(-?[1-7])?|(00[1-9]|0[1-9]\d|[12]\d{2}|3([0-5]\d|6[1-6])))([T\s]((([01]\d|2[0-3])((:?)[0-5]\d)?|24\:?00)([\.,]\d+(?!:))?)?(\17[0-5]\d([\.,]\d+)?)?([zZ]|([\+-])([01]\d|2[0-3]):?([0-5]\d)?)?)?)?$/;
    describe("Basic Assumptions", function () {
        it("has loaded TroubleTicket code", function () {
            expect(TroubleTicket).toBeDefined();
        });
        it("has TroubleTicket.controller.Ticket", function () {
            expect(TroubleTicket.controller.Ticket).toBeDefined();
        });
    });
    describe("TroubleTicket Ticket Controller - Add", function () {
        "use strict";
        var file = {
                filename: 'Testfile.jpg',
                mimetype: 'image/jpeg',
                base64Data: 'TEST',
                url: '',
                getData : function() {
                    return this;
                }
                
            },
            application,
            controller,
            viewport,
            formPanel;

        beforeEach(function () {
            application = application || TroubleTicket.getApplication();
            controller = controller || application.controllers.get('Ticket');
            viewport = viewport || Ext.ComponentQuery.query('.viewport')[0];
        });

        it("should define the global TroubleTicket namespace", function () {
            expect(TroubleTicket).toBeDefined();
            expect(typeof TroubleTicket).toEqual('object');
        });

        it("should have the TT.postTicket object", function () {
            expect(TroubleTicket.vars.post).toBeDefined();
            expect(typeof TroubleTicket.vars.post).toEqual('object');
        });

        it("has the controller", function () {
            expect(controller).toBeDefined();
        });

        it("should have the open action method", function () {
            expect(typeof controller.actionOpen).toEqual('function');
        });

        it("the open action should open the form", function () {
            controller.actionOpen();
            formPanel = viewport.down('.ticketForm');
            expect(formPanel).toBeDefined();
            expect(formPanel).not.toBeNull();
        });

        it("the form panel should have valid record", function () {
            var record = formPanel.getForm().getRecord();
            expect(record).toBeDefined();
            expect(record).not.toBeNull();
            expect(record.validate(false).isValid()).toBeTruthy();
        });
        it("the form should contain the IMSI field with valid value", function () {
            var form = formPanel.getForm(),
                field = form.findField('imsi');
            expect(field).toBeDefined();
            expect(field.getValue()).toMatch(/^\d{15}$/);
        });
        it("the form should contain the MSISDN field with valid value", function () {
            var form = formPanel.getForm(),
                field = form.findField('msisdn');
            expect(field).toBeDefined();
            expect(field.getValue()).toMatch(/^(|\d{1,128})$/);
        });
        it("the form should contain the startTime field with valid value", function () {
            var form = formPanel.getForm(),
                field = form.findField('startTime');
            expect(field).toBeDefined();
            expect(field.getValue()).toMatch(dateValidatorRegexp);
        });
        it("the form should contain the endTime field with valid value", function () {
            var form = formPanel.getForm(),
                field = form.findField('endTime');
            expect(field).toBeDefined();
            expect(field.getValue()).toMatch(dateValidatorRegexp);
        });
        it("the form should contain the description field with valid value", function () {
            var form = formPanel.getForm(),
                field = form.findField('description');
            expect(field).toBeDefined();
            expect(field.getValue()).toMatch(/\w/);
        });
        it("the form should contain the CQM attachment field", function () {
            var form = formPanel.getForm(),
                field = form.findField('attachments');
            expect(field).toBeDefined();
            expect(field).not.toBeNull();
            expect(field instanceof Ext.ux.form.field.Attachment).toBeTruthy();
            expect(typeof field.getValue() === 'object').toBeTruthy();
            expect(form.getRecord().getData(true).attachments.length).toBeGreaterThan(0);
        });

        it("should have the deleteAttachment action method", function () {
            expect(typeof controller.deleteAttachment).toEqual('function');
        });

        var getAttachments = function(fields) {

            var attachments = []
            Ext.each(fields, function(value, index, array) {
                if(value.name.indexOf('attachments') !== -1) {
                    attachments.push(value);
                }
            },this);

            return attachments;
        };

        var getAttachmentsWithDeleteLink = function(attachments) {

            var elements = [];

            Ext.each(attachments, function(value, index, array) {

                elements = value.el.query('a[role=remove]');

            },this);

            return elements;
        };

        it("the attachment that are not in the server should be removable", function () {

            var form = formPanel.getForm(),
                field = form.findField('attachments'),
                event,
                el;
            expect(field).toBeDefined();
            el = field.el.query('a[role=remove]');
            expect(el.length).toEqual(1);
            
            formPanel.addAttachmentFile(file);
            
            var fields = form.getFields().items;
            var attachments = getAttachments(fields);
            
            el = getAttachmentsWithDeleteLink(attachments);
            expect(el.length).toBeGreaterThan(0);
            el = el[0];

            if (document.createEvent) {
                event = document.createEvent("HTMLEvents");
                event.initEvent("click", true, true);
            } else {
                event = document.createEventObject();
                event.eventType = "click";
            }
            event.eventName = 'click';
            event.memo = {};
            if (document.createEvent) {
                el.dispatchEvent(event);
            } else {
                el.fireEvent("on" + event.eventType, event);
            }

            var attachments = getAttachments(fields);
            el = getAttachmentsWithDeleteLink();

            //the attachment added before was removed
            expect(el[0]).toBeUndefined();
        });

        it("the deleteAttachment action should remove the attachment in the server side", function () {

            expect(controller.getSelectedTicketId()).not.toBeUndefined();

            spyOn(Ext.Ajax, "request").andCallFake(function(e) {
                e.success({});
            });

            controller.deleteAttachment(file,function() {
                //TODO it is needed to mock the responses to call the success callback
                expect(true).toBe(true);
            });
        });

        it("the open action should open the form, delete attachment flag is true", function () {

            TroubleTicket.vars.config.ticketDeleteAttachment = true;

            controller.actionOpen();
            formPanel = viewport.down('.ticketForm');
            expect(formPanel).toBeDefined();
            expect(formPanel).not.toBeNull();
        });

        it("the attachment should be removable", function () {
            var form = formPanel.getForm(),
                field = form.findField('attachments'),
                event,
                el;
            expect(field).toBeDefined();
            el = field.el.query('a[role=remove]');
            expect(el.length).toBeGreaterThan(0);
            el = el[0];
            if (document.createEvent) {
                event = document.createEvent("HTMLEvents");
                event.initEvent("click", true, true);
            } else {
                event = document.createEventObject();
                event.eventType = "click";
            }
            event.eventName = 'click';
            event.memo = {};
            if (document.createEvent) {
                el.dispatchEvent(event);
            } else {
                el.fireEvent("on" + event.eventType, event);
            }
            expect(form.findField('attachments')).toBeNull();

            TroubleTicket.vars.config.ticketDeleteAttachment = false;
        });

        it("TicketForm#addAttachmentFile shouldn't add new attachment field with invalid file JSON", function () {
            var attachmentFieldSet = formPanel.getAttachmentCtr(),
                itemsLength = attachmentFieldSet.items.length,
                f = Ext.apply({}, file),
                field;
            delete f.mimetype;
            formPanel.addAttachmentFile(f);
            expect(attachmentFieldSet.items.length).toEqual(itemsLength);
            expect(Ext.Msg.isHidden()).toBeFalsy();
            Ext.Msg.hide();
        });
        it("TicketForm#addAttachmentFile should add new attachment field with valid file JSON", function () {
            var attachmentFieldSet = formPanel.getAttachmentCtr(),
                itemsLength = attachmentFieldSet.items.length,
                field;
            formPanel.addAttachmentFile(file);
            field = attachmentFieldSet.items.getAt(attachmentFieldSet.items.length - 2);
            expect(attachmentFieldSet.items.length).toEqual(itemsLength + 1);
            expect(field instanceof Ext.ux.form.field.Attachment).toBeTruthy();
            expect(field.getValue()).toEqual(file);
        });
        it("the form shouldn't be saved with empty title field", function () {
            var form = formPanel.getForm(),
                btn = formPanel.getSubmitBtn(),
                field = form.findField('title');
            field.setValue('');
            btn.fireEvent('click', btn);
        });
        it("the controller#saveRecord method should be called on valid form submit", function () {
            var form = formPanel.getForm(),
                btn = formPanel.getSubmitBtn(),
                field = form.findField('title'),
                tmpFn = controller.saveRecord;
            field.setValue('Test');
            controller.saveRecord = function(){};
            spyOn(controller, 'saveRecord');
            btn.fireEvent('click', btn);
            expect(controller.saveRecord).toHaveBeenCalled();
            controller.saveRecord = tmpFn;
        });

        it("should create ticket model instance with 'createTicketModelFromPostVars'", function () {
            var model = controller.createTicketModelFromPostVars();
            expect(model instanceof TroubleTicket.model.ticket.Ticket).toBeTruthy();
        });

    });
}());