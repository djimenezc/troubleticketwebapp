/*
    This file is generated and updated by Sencha Cmd. You can edit this file as
    needed for your application, but these edits will have to be merged by
    Sencha Cmd when upgrading.
*/

//Ext.Loader.setConfig({
//    enabled : true,
//    disableCaching : false
//});

//TODO IE8 issue move to a more suitable place
//http://stackoverflow.com/questions/3629183/why-doesnt-indexof-work-on-an-array-ie8
if (!Array.prototype.indexOf)
{
    Array.prototype.indexOf = function(elt /*, from*/)
    {
        var len = this.length >>> 0;

        var from = Number(arguments[1]) || 0;
        from = (from < 0)
            ? Math.ceil(from)
            : Math.floor(from);
        if (from < 0)
            from += len;

        for (; from < len; from++)
        {
            if (from in this &&
                this[from] === elt)
                return from;
        }
        return -1;
    };
}

Ext.Loader.onReady(function() {

    //Set the global network timeout to the value defined in the config object
    Ext.Ajax.timeout = TroubleTicket.vars.config.operationTimeout;
    Ext.override(Ext.data.proxy.Ajax, { timeout:Ext.Ajax.timeout });

    // global AJAX request exception handler
    Ext.Ajax.on('requestexception', function(conn, response, options) {

        if(response.status === 0 || response.status === 408) {
            //Timeout in the frontend or in the backend
            //TODO i18n
            TroubleTicket.getApplication().fireEvent('showMessage',
                {title: 'Network error',msg : "Operation Timed Out - Please Try Again"});
        }
        else {
            TroubleTicket.getApplication().fireEvent('errorHandler',response);
        }
    });

});


Ext.application({
    name: 'TroubleTicket',
    extend: 'TroubleTicket.Application',
    autoCreateViewport: true,
    paths: {
        'Ext.locale': 'ext/locale',
        'Ext.ux': 'vendor/ext/ux',
        'Ext.overrides': 'overrides',
        'i18next': 'vendor/i18next/i18next-1.7.1.js'
    }
});
