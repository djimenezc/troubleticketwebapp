/**
 * Created by nickrogers on 19/12/2013.
 */
Ext.define('Ext.overrides.data.proxy.ServerOverride', {
    override: 'Ext.data.proxy.Server',

    processResponse: function (success, operation, request, response, callback, scope) {
        operation.serverResponse = response;
        this.callParent(arguments);
    }
});