/**
 * This class will override the Ext.data.writer.Writer class to add the associated data from the record to the request.
 */
Ext.define('Ext.overrides.data.writer.Writer', {
    override: 'Ext.data.writer.Writer',
    /**
     * Formats the data for each record before sending it to the server. This
     * method should be overridden to format the data in a way that differs from the default.
     * Overridden to add the associated data to the request.
     * @param {Ext.data.Model} record The record that we are writing to the server.
     * @param {Ext.data.Operation} [operation] An operation object.
     * @return {Object} An object literal of name/value keys to be written to the server.
     * By default this method returns the data property on the record.
     */
    getRecordData: function (record, operation) {
        var data = this.callOverridden(arguments);
        if (record.associations.length) {
            Ext.applyIf(data, record.getAssociatedData());
        }
        return data;
    }
});