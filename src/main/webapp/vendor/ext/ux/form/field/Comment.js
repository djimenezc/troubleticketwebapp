Ext.define('Ext.ux.form.field.Comment', {
    extend: 'Ext.form.field.Base',
    requires: [
        'TroubleTicket.util.String'
    ],
    alias: ['widget.commentfield', 'widget.comment'],
    alternateClassName: 'Ext.form.Comment',


    /**
     * @cfg {String} [fieldCls="x-form-attachment-field"]
     * The default CSS class for the field.
     */
    fieldCls: Ext.baseCSSPrefix + 'form-comment-field',

    fieldBodyCls: Ext.baseCSSPrefix + 'form-comment-field-body',

    initComponent: function () {
        if (Ext.isString(this.value)) {
            this.value = Ext.JSON.decode(this.value);
        }

        this.value.description = TroubleTicket.util.String.newLineToBr(this.value.description);

        this.fieldSubTpl =  new Ext.XTemplate([
            '<article class="tt-comment-ctr">',
            '<footer class="tt-comments-title">{value.created}</footer>',
            '<section>{value.description}</section>',
            '</article>',

            {
                compiled: true,
                disableFormats: true,
            }
        ]);

        return this.callParent();
    },
    /**
     * Returns the raw value of the field, without performing any normalization, conversion, or validation. To get a
     * normalized and converted value see {@link #getValue}.
     * @return {String} value The raw String value of the field
     */
    getRawValue: function () {
        "use strict";
        return this.value;
    },
    /**
     * Converts a mixed-type value to a raw representation suitable for displaying in the field. This allows controlling
     * how value objects passed to {@link #setValue} are shown to the user, including localization. For instance, for a
     * {@link Ext.form.field.Date}, this would control how a Date object passed to {@link #setValue} would be converted
     * to a String for display in the field.
     *
     * See {@link #rawToValue} for the opposite conversion.
     *
     * The base implementation simply does a standard toString conversion, and converts {@link Ext#isEmpty empty values}
     * to an empty string.
     *
     * @param {Object} value The mixed-type value to convert to the raw representation.
     * @return {Object} The converted raw value.
     */
    valueToRaw: function (value) {
        "use strict";
        return Ext.isString(value) ? Ext.JSON.decode(value) : value;
    },
    /**
     * Creates and returns the data object to be used when rendering the {@link #fieldSubTpl}.
     * @return {Object} The template data
     * @template
     */
    getSubTplData: function () {
        var data = this.callParent(arguments);
        Ext.apply(data.value, {
            created: TroubleTicket.util.Date.formatDateTime(data.value.created)
        });
        return data;
    },
    renderer: function () {
        return TroubleTicket.util.String.newLineToBr(value);
    },
    /**
     * Override the original method to check the full object with the mandatory fields
     * @returns {boolean}
     */
    isValid: function () {
        return true;
    }
});