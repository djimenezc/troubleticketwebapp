Ext.define('Ext.ux.form.field.StatusComboBox', {
    extend: 'Ext.form.field.ComboBox',
    alias: ['widget.statuscombobox', 'widget.statusbox'],
    alternateClassName: 'Ext.form.Comment',
    constructor: function (cfg) {
        var statuses = TroubleTicket.config.ticketStates,
            data = [];
        if (!cfg.data && !cfg.store) {
            Ext.Object.each(statuses, function (key, value) {
                if(!value.notSelectable) {
                    data.push([
                        key,
                        TroubleTicket.util.i18n.translate('status.' + key)
                    ]);
                }
            });
            cfg.store = data;
        }
        this.callParent(arguments);
    }
});