Ext.define('Ext.ux.form.field.Attachment', {
    extend: 'Ext.form.field.Base',
    alias: ['widget.attachmentfield', 'widget.attachment'],
    alternateClassName: 'Ext.form.Attachment',
    previewActionLabel: TroubleTicket.util.i18n.translate('attachment.preview.msg'),
    removeActionLabel: TroubleTicket.util.i18n.translate('attachment.remove.msg'),
    fieldSubTpl: [
        '<div id="{id}" role="input" ',
        '<tpl if="fieldStyle"> style="{fieldStyle}"</tpl>',
        ' class="{fieldCls}">',
        '<span class="' + Ext.baseCSSPrefix + 'filename">',
        '<tpl if="value.url"><a href="{value.contentURL}" target="_blank"></tpl>',
        '{value.filename}',
        '<tpl if="value.url"></a></tpl>',
        '</span>',
        '<span class="' + Ext.baseCSSPrefix + 'actions">',
        //'<a href="javascript:void(0);" role="preview">{previewActionLabel}</a>',
        //'<span class="' + Ext.baseCSSPrefix + 'action-separator"></span>',
        '<tpl if="value.showDeleteLink"><a href="javascript:void(0);" role="remove">{removeActionLabel}</a></tpl>',
        '</span>',
        '</div>',
        {
            compiled: true,
            disableFormats: true
        }
    ],

    /**
     * @cfg {String} [fieldCls="x-form-attachment-field"]
     * The default CSS class for the field.
     */
    fieldCls: Ext.baseCSSPrefix + 'form-attachment-field',

    fieldBodyCls: Ext.baseCSSPrefix + 'form-attachment-field-body',

    initComponent: function () {
        if (Ext.isString(this.value)) {
            this.value = Ext.JSON.decode(this.value);
        }
        return this.callParent();
    },
    afterRender: function () {
        var res = this.callParent(arguments),
            previewEl = this.el.query('[role=preview]'),
            removeEl = this.el.query('[role=remove]');
        previewEl = previewEl instanceof Array ? previewEl[0] || null : previewEl;
        if (previewEl) {
            previewEl = new Ext.dom.Element(previewEl);
            previewEl.on('click', this.onPreviewElClick, this);
        }
        removeEl = removeEl instanceof Array ? removeEl[0] || null : removeEl;
        if (removeEl) {
            removeEl = new Ext.dom.Element(removeEl);
            removeEl.on('click', this.onRemoveElClick, this);
        }
        return res;
    },
    /**
     * Event handler for the preview link's click event.
     * @private
     */
    onPreviewElClick: function () {
        "use strict";
        this.fireEvent('preview');
    },
    /**
     * Event handler for the remove link's click event. Destroy the field only if it is not readonly.
     * @private
     */
    onRemoveElClick: function () {
        "use strict";
        var file = this.getRawValue();
        if(file.id) {
            //Remove file remotely, pass the file and the method to execute in the callback if everything go well
            TroubleTicket.getApplication().fireEvent('deleteAttachment', file, this.destroy,this);
        } else if (!this.readOnly) {
            this.destroy();
        }
    },
    /**
     * Returns the raw value of the field, without performing any normalization, conversion, or validation. To get a
     * normalized and converted value see {@link #getValue}.
     * @return {String} value The raw String value of the field
     */
    getRawValue: function () {
        "use strict";
        return this.value;
    },
    /**
     * Converts a mixed-type value to a raw representation suitable for displaying in the field. This allows controlling
     * how value objects passed to {@link #setValue} are shown to the user, including localization. For instance, for a
     * {@link Ext.form.field.Date}, this would control how a Date object passed to {@link #setValue} would be converted
     * to a String for display in the field.
     *
     * See {@link #rawToValue} for the opposite conversion.
     *
     * The base implementation simply does a standard toString conversion, and converts {@link Ext#isEmpty empty values}
     * to an empty string.
     *
     * @param {Object} value The mixed-type value to convert to the raw representation.
     * @return {Object} The converted raw value.
     */
    valueToRaw: function (value) {
        "use strict";
        return Ext.isString(value) ? Ext.JSON.decode(value) : value;
    },
    /**
     * Creates and returns the data object to be used when rendering the {@link #fieldSubTpl}.
     * @return {Object} The template data
     * @template
     */
    getSubTplData: function () {
        var data = this.callParent(arguments);
        Ext.applyIf(data, {
            previewActionLabel: this.previewActionLabel,
            removeActionLabel: this.removeActionLabel
        });
        return data;
    },
    /**
     * Override the original method to check the full object with the mandatory fields
     * @returns {boolean}
     */
    isValid: function () {
        var res = this.callParent(arguments),
            value = this.getValue();
        return res && /\w{1,}/.test(value.mimetype || '') && /\w{1,}/.test(value.filename || '');
    }
});