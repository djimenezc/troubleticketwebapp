/**
 * @class Ext.ux.data.reader.DynamicJson
 * This class is an extended class of the Json Reader to handle the extra custom fields in our Ticket model
 */
Ext.define('Ext.ux.data.reader.DynamicJson', {
    extend: 'Ext.data.reader.Json',
    alternateClassName: 'Ext.data.DynamicJsonReader',
    alias : 'reader.dynamicjson',
    /**
     * Returns extracted, type-cast rows of data.
     * @param {Object[]/Object} root from server response
     * @return {Array} An array of records containing the extracted data
     * @private
     */
    extractData : function (root) {
        "use strict";
        var me = this,
            Model   = me.model,
            length  = root.length,
            records = new Array(length),
            node,
            record,
            i;

        if (!root.length && Ext.isObject(root)) {
            root = [root];
            length = 1;
        }

        for (i = 0; i < length; i += 1) {
            node = root[i];
            if (node.isModel) {
                // If we're given a model instance in the data, just push it on
                // without doing any conversion
                records[i] = node;
            } else {
                // Create a record with an empty data object.
                // Populate that data object by extracting and converting field values from raw data.
                // Must pass the ID to use because we pass no data for the constructor to pluck an ID from
                records[i] = record = new Model(node);

                if (me.implicitIncludes && record.associations.length) {
                    me.readAssociated(record, node);
                }
            }
        }
        return records;
    }
});